#!/bin/bash

DIR=`dirname $0`

cd $DIR
wget https://code.jquery.com/jquery-1.12.4.min.js 
ln -s jquery-1.12.4.min.js jquery.min.js
wget https://code.jquery.com/ui/1.12.0/jquery-ui.min.js
wget https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css
wget https://github.com/mikecao/flight/archive/v1.2.21.zip
unzip v1.2.21.zip
rm v1.2.21.zip
ln -s flight-1.2.21/flight flight
wget https://github.com/smarty-php/smarty/archive/v3.1.29.zip
unzip v3.1.29.zip
rm v3.1.29.zip
ln -s smarty-3.1.29 smarty
