<?php
/**
 * @file member.php
 * Member representation
 */

/**
 * @class
 * A link to locations
 */
class Powon_Location_Join extends Powon_Entity_Join
{
	/**
	 * Constructor
	 *
	 * @param string $parent
	 *   Reference to the parent entity
	 * @param string $lookup
	 *   A lookup table if needed
	 * @param boolean $fetch
	 *   Whether or not to fetch on construction
	 */
	function __construct (&$parent, $lookup=NULL, $fetch=TRUE)
	{   
		parent::__construct('Location', $parent, $lookup, $fetch, 'Powon_Location', FALSE);
	}

	/**
	 * Generate the query for fetching data
	 */
	function make_query($cols = array())
	{
		$fields = (empty($cols)) ? 'ref.*, lk.*' : 'ref.' . implode(', ref.', $cols);
		$query = "SELECT {$fields} FROM {$this->_name} ref";
		if (!is_null($this->_lookup))
			$query .= " NATURAL JOIN {$this->_lookup} lk WHERE lk.{$this->_parent->key()} = :id";
		else
			$query .= " WHERE lk.{$this->_parent->key()} = :id";
		return $query;
	}

}

/**
 * @class
 * Location object intended to be attached to some other primary object
 */
class Powon_Location extends Powon_Entity
{

	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as a unique id
	 */
	function __construct($id=NULL)
	{
		parent::__construct($id, 'locID', 'Location', array(), FALSE);
		$this->link('MemberLocation');
		$this->_tpl = 'address.tpl';
	}

	/**
	 * Is the location public
	 */
	function is_public()
	{
		return (is_null($this->isPrivate)) ? FALSE : !$this->isPrivate;
	}

	/**
	 * Retrieve the primary address for the given uID
	 */
	static function primary_address($uID)
	{
		$uID = filter_var($uID, FILTER_SANITIZE_STRING);
		$lid = Powon::qry("SELECT locID FROM MemberLocation WHERE uID = $uID AND isPrimary = '1' LIMIT 1")->fetchColumn();
		return self::instance($lid);
	}

	/**
	 * Generate a title
	 *
	 * @retval string
	 *   A title for this entity
	 */
	function format_title()
	{
		$title = "$this->label $this->city";
		return $title;
	}

	/**
	 * Register template variables
	 *
	 * @retval array
	 *   key value pairs for assignment to a view
	 */
	function template_vars()
	{   
		$vars = array(
			'loc_template' => $this->_tpl,
			'loc_label' => $this->format_title(),
			'loc_street' => $this->street,
			'loc_city' => $this->city,
			'loc_region' => $this->region,
			'loc_prov' => $this->prov,
			'loc_country' => $this->country,
			'loc_code' => $this->code,
		);
		return $vars;
	}

	/**
	 * Public access to template vars
	 */
	function embed_template_vars()
	{
		return $this->template_vars();
	}

	/**
	 * Register routes
	 */
	static function routing()
	{   
		Flight::route('GET /location/@id/edit', array('Powon_Location', 'edit'));
		Flight::route('POST /location/@id/edit', array('Powon_Location', 'edit'));
	}

    /**
     * Edit a location
     *
     * @param $id
     *   Location id
     */
    static function edit($id)
    {
		parent::edit($id, 'locID', 'Location');
    }

    /**
     * Override action links
     */
    function action_link($op) { return "/location/{$this->id()}/{$op}"; }

	/**
	 * Retrieve linked user id
	 */
	function uID() { return $this->_linked['MemberLocation']->item(0)->uID; }

	/**
	 * Location access control
	 *
	 * @param string $op
	 *   Operation being requested
	 * @retval boolean
	 *   Whether the current user can access the entity
	 */
	function access($op='view')
	{
		$user = Powon::current_user();
		$acc = FALSE;
		switch ($op)
		{
			case 'embed':
				$acc = $user->is_user(); // Objects embedding locations must perform their own access checks
				break;

			case 'edit':
			case 'view':
			case 'admin':
				$acc = ($user->is_admin() || $user->id() == $this->uID());
				break;

			default:
				$acc = FALSE;
				break;
		}
		return $acc;
	}

}

?>
