<?php

/*
 * Strict validation will use stricter validation for a variety of values
 *
 *   - email addresses must exist
 */
define('POWON_STRICT_VALIDATION', FALSE);

/*
 * System email address
 */
define('POWON_STRICT_VALIDATION', "powon@example.com");

/*
 * If powon is hosted in a subdirectory, configure it here
 */
define('POWON_SUBDIR', '');

/*
 * Set to true to activate debug output
 */
define('POWON_DEBUG', FALSE);

function powon_conf()
{
	static $conf;
	if (!isset($conf))
	{
		$conf = new stdClass();

		// Set db credentiels
		$conf->dbuser = "username";
		$conf->dbpwd = "password";
		$conf->dbhost = "localhost";
		$conf->dbname = "database";

		// Set password salts
		$conf->blowfish_salt = '$2a$04$twentytwocharactersalt$';
		$conf->md5_salt = '$1$eightcha$';
	}
	return $conf;
}

?>
