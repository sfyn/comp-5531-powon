<?php
/**
 * @file powon.php
 * The powon base class
 */

// Powon constants
define('POWON_DEFAULT_USER_RELATIONSHIP', 0);

/**
 * @class
 * Powon utility functions and "globals"
 */
class Powon
{
	static private $log;
	static private $path;
	static private $conf;

	/**
	 * Prevent this static class from being constructed
	 */
	private function __construct() {}

	/**
	 * Install powon
	 *
	 * @param string $p
	 *   Base path to use
	 */
	static function install($p)
	{
		self::$path = $p;
		self::load('powon/conf');
		self::$conf = powon_conf();

		self::load('lib/smarty/libs/Smarty.class');

		// Load powon components
		self::load('powon/form');
		self::load('powon/data');
		self::load('powon/user');
		self::load('powon/location');
		self::load('powon/member');
		session_start();

		$db = self::db();
		$smarty = new Smarty;
		$smarty->setTemplateDir (self::$path . '/tpl/src');
		$smarty->setCompileDir (self::$path . '/tpl/comp');
		$smarty->setConfigDir (self::$path . '/tpl/conf');
		$smarty->setCacheDir (self::$path . '/tpl/cache');

		// Check no member exists
		$sql = $db->prepare('SELECT COUNT(uID) FROM Member');
		self::qry_wrap($sql);
		if ($sql->fetchColumn() == '0')
		{
			$first = Powon_Member::instance(NULL);
			$first->build_form(array('joined', 'status', 'priv', 'avatar'));
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$first->get_form()->validate();
				$data = $first->get_form()->data();
				$data['priv'] = 'admin';
				$first->save($data);
				self::redirect(POWON_SUBDIR);
			}
			else
			{
				$smarty->assign('form', $first->get_form()->generate());
				$smarty->display('install.tpl');
			}
		}
		else
			self::redirect(POWON_SUBDIR);

	}

	

	/**
	 * Initialize powon
	 *
	 * @param string $p
	 *   Base path to use
	 */
	static function init($p)
	{
		self::$path = $p;
		self::load('powon/conf');
		self::$conf = powon_conf();
		self::load('lib/flight/Flight');

		// Configure flight to use smarty
		self::load('lib/smarty/libs/Smarty.class');
		Flight::register('view', 'Smarty', array(), function($smarty){
			$smarty->setTemplateDir (self::$path . '/tpl/src');
			$smarty->setCompileDir (self::$path . '/tpl/comp');
			$smarty->setConfigDir (self::$path . '/tpl/conf');
			$smarty->setCacheDir (self::$path . '/tpl/cache');
		});

		// Load powon components
		self::load('powon/form');
		self::load('powon/data');
		self::load('powon/media');
		self::load('powon/user');
		self::load('powon/location');
		self::load('powon/member');
		self::load('powon/group');
		session_start();

		// Register routes
		Flight::route('GET /test', array('Powon', 'display_test'));
		Flight::route('POST /test', array('Powon', 'display_test'));
		Flight::route('GET /', array('Powon', 'posts'));
		Flight::route('GET /admin', array('Powon', 'admin_links'));
		Powon_User::routing();
		Powon_Entity::routing();
		Powon_Interest::routing();
		Powon_Media::routing();
		Powon_Member::routing();
		Powon_Location::routing();
		Powon_Group::routing();

		Flight::start();
	}

	/**
	 * Front page
	 */
	function posts()
	{   
		$media = new Powon_Entity_Collection('Media', "SELECT m.* FROM Media m NATURAL JOIN PowonMedia", TRUE, 'Powon_Media');
		$posts = array();
		foreach ($media->items() as $post)
		{
			$posts[] = $post->template_vars();
		}
		self::render('posts.tpl', array(
			'title' => 'Welcome to Powon',
			'posts' => $posts
		));
	}

	/**
	 * Administrative stuff
	 */
	static function admin_links()
	{
		if (self::access_found(TRUE, self::current_user()->is_admin()))
		{
			self::render('generic-page.tpl', array(
				'title' => 'Admin',
				'content' => "<ul>\n\t<li><a href='" . POWON_SUBDIR . "/admin/member'>Members</a></li>\n<li><a href='" .POWON_SUBDIR . "/admin/post'>Post to main page</a></li>\n</ul>\n"
			));
		}
	}

	/** 
	 * Access denied 
	 */ 
	static function access_denied() 
	{
		header("HTTP/1.1 403 Forbidden");
		self::render('entity.tpl', array('entity_title'=>'Access Denied', 'entity_data'=>array()));
	} 

	/**
	 * Not found
	 */
	static function not_found()
	{
		header("HTTP/1.0 404 Not Found");
		self::render('entity.tpl', array('entity_title'=>'Not Found', 'entity_data'=>array()));
	}

	/**
 	 * Test page
	 */
	static function display_test()
	{
		$tpl = 'posts.tpl';
		$vars= array('title' => 'Welcome to Powon');
		self::render($tpl, $vars);
	}

	/**
	 * Confirmation dialog
	 */
	static function display_confirm($message, $yes, $no)
	{
		$form = self::confirm_form($yes, $no);
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$form->validate();
			$yes = is_null(filter_input(INPUT_POST, 'no', FILTER_SANITIZE_STRING));
			if ($yes)
				self::redirect(POWON_SUBDIR . $form->data('confirmed'));
			else
				self::redirect(POWON_SUBDIR . $form->data('cancelled'));
		}
		else
		{
			$vars = array(
				'title' => $message,
				'form' => $form->generate(),
			);
			self::render('generic-form.tpl', $vars);
		}
	}

	/**
	 * Confirmation form
	 */
	static function confirm_form($yes, $no)
	{
		$confirm = new Powon_Form('confirm-form', 'Yes');
		$confirm->add('hidden', 'confirmed', $yes);
		$confirm->add('hidden', 'cancelled', $no);
		$confirm->add('submit', 'no', '', 'No');
		return $confirm;
	}

	/**
	 * Convenience alias to retrieve the current user
	 */
	static function current_user()
	{
		self::load('powon/user');
		return Powon_User::current_user();
	}

	/**
	 * Provide a redirect value for use in a link
	 */
	static function redirect_value() { return (isset($_SERVER['REQUEST_URI'])) ? '?redirect=' . parse_url($_SERVER['REQUEST_URI'])['path'] : ''; }

	/**
	 * Redirect the user
	 */
	static function redirect($to=NULL, $not=NULL)
	{
		if (is_null($to))
		{
			if ($dest = filter_input(INPUT_GET, 'redirect', FILTER_SANITIZE_URL))
				$to = $dest;
			else if (isset($_SERVER['HTTP_REFERER']))
				$to = $_SERVER['HTTP_REFERER'];
		}
		$not = (is_null($not) && isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : $not;
		if (!is_null($to))
		{
			if (is_null($not) || (parse_url($to, PHP_URL_PATH) != $not))
				{
					header("Location: {$to}");
					exit();
				}
		}
	}

	/**
	 * Load a file or module
	 *
	 * @param string $what
	 *   What to require_once
	 */
	static function load($what)
	{
		require_once (self::$path . "/{$what}.php");
	}

	/**
	 * Return a sanitized version of the provided string, for use with db queries
	 *
	 * @param string $in
	 *   String to sanitize
	 * @retval string
	 *   String stripped of all but alphanumeric characters and underscores
	 */
	static function dbsafe($in)
	{
		static $rexp = '/[^A-Za-z0-9_]+/';
		return preg_replace($rexp, '', filter_var($in, FILTER_SANITIZE_STRING));
	}

	/**
	 * Helper to retrieve the last insert id
	 *
	 * @param $table
	 *   Mysql column name in the form "`table`.`column`"
	 */
	static function db_last_id($tablekey=FALSE)
	{
		$id = NULL;
		if ($tablekey)
		{
			$tablekey = explode('.', $tablekey);
			$id = self::qry("SELECT MAX({$tablekey[1]}) FROM {$tablekey[0]}")->fetchColumn();
		}
		else
		{
			$id = self::db()->lastInsertId();
			if (!$id)
				$id = self::qry("SELECT LAST_INSERT_ID()")->fetchColumn();
		}
		return $id;
	}

	/**
	 * PDO object to connect to the database
	 */
	static function db()
	{
		return new PDO('mysql:host=' . self::$conf->dbhost . ';dbname=' . self::$conf->dbname, self::$conf->dbuser, self::$conf->dbpwd);
	}

	/**
	 * Standard handling of a query
	 *
	 * @param $sql
	 *   Query string
	 * @param $bind
	 *   Paramaters to bind to the pdo statement
	 */
	static function qry($sql, $bind=array())
	{
		$db = Powon::db();
		$statement = $db->prepare($sql);
		foreach ($bind as $p => $v)
		{
			$statement->bindValue($p, $v, PDO::PARAM_STR);
		}
		return self::qry_wrap($statement);
	}


	/**
	 * Standard handling of a pdo statement
	 *
	 * @param $statement
	 *   A PDOStatement object with a prepared query
	 */
	static function qry_wrap(&$statement)
	{
		$statement->execute();
		Powon::log('Query: ' . $statement->queryString);

		if ($statement->errorCode() != 0)
		{
			if (self::current_user()->is_admin() || POWON_DEBUG)
				throw new Exception('Database error: ' . $statement->errorInfo()[2] . "\nQuery was: " . $statement->queryString, E_ERROR);
			else
				throw new Exception('Database error.', E_ERROR);
		}
		return $statement;
	}

	/**
	 * Access the path
	 *
	 * @retval string
	 *   the currently set path
	 */
	static function path()
	{
		return self::$path;
	}

	/**
	 * Write a line to the log or access it
	 *
	 * @param string $line
	 *   Line to write
	 */
	static function log($line = FALSE)
	{
		if (!isset(self::$log))
		{
			require_once (self::$path . '/powon/log.php');
			self::$log = new Powon_Log();
		}
		if ($line)
			self::$log->wl($line);
		else
			return self::$log;
	}

	/**
	 * Boolean check for access and found
	 */
	static function access_found($found, $access)
	{
		$result = FALSE;
		if (!$found)
			self::not_found();
		else if (!$access)
			self::access_denied();
		else
			$result = TRUE;
		return $result;
	}

	/**
	 * Render a page, if it may be accessed and found
	 */
	static function render_if($found, $access, $tpl, $vars)
	{
		if (self::access_found($found, $access))
		{
			if (is_callable($vars))
				$vars = call_user_func($vars);
			self::render($tpl, $vars);
		}
			
	}

	/**
	 * Render a page
	 *
	 * @param string $tpl
	 *   Template file to use
	 * @param array $vars
	 *   Template variables to assign - may override previously assigned values
	 * @param boolean $nav
	 *   Set to false to suppress navigational elements on this page
	 */
	static function render($tpl, $vars=array(), $nav=TRUE)
	{
		$user = self::current_user();

		// Things that may be present on any given page
		$login = ($nav && $user->is_user()) ? '' : Powon_User::login_form()->generate();
		Flight::view()->assign('user_avatar', $user->avatar()->html());
		Flight::view()->assign('log_lines', self::log()->lines());
		Flight::view()->assign('login_form', $login);

		// Access control related values
		Flight::view()->assign('admin', $user->is_admin());
		Flight::view()->assign('logged_in', $user->is_user());
		Flight::view()->assign('show_nav', $nav);
		Flight::view()->assign('inline_js', '');
		Flight::view()->assign('powon_subdir', POWON_SUBDIR);

		foreach ($vars as $var => $val)
		{
			Flight::view()->assign($var, $val);
		}

		Flight::view()->display($tpl);
	}

	/**
	 * Template a link html element
	 * 
	 * @param string $href
	 *   Link destination
	 * @param string $label
	 *   The label to use
	 * @param array $class
	 *   Array of classes to use
	 * @retval string
	 *   A ready to use html link
	 */
	static function tpl_link($href, $label=NULL, $class = array())
	{
		$label = (is_null($label)) ? $href : $label;
		$class = (empty($class)) ? '' : ' class="' . implode(' ', $class);
		return "<a href=\"$href\"$class>$label</a>";
	}

	/**
	 * Date diff
	 *
	 * @retval string
	 *   How long since the given date
	 */
	static function format_since($date)
	{
		$joined = ($date instanceof DateTime) ? $date : new DateTime($date);
		$now = new DateTime();
		$diff = $now->diff($joined);
		if ($diff->y)
		{
			$quantity = $diff->y;
			$unit = "year";
		}
		else if ($diff->m)
		{
			$quantity = $diff->m;
			$unit = "month";
		}
		else if ($diff->d)
		{
			$quantity = $diff->d;
			$unit = "day";
		}
		else if ($diff->h)
		{
			$quantity = $diff->h;
			$unit = "hour";
		}
		else if ($diff->i)
		{
			$quantity = $diff->i;
			$unit = "minute";
		}
		else
		{
			$quantity = $diff->s;
			$unit = "second";
		}
		if ($quantity > 1)
			$unit .= 's';
		return "{$quantity} {$unit}";
	}
}
?>
