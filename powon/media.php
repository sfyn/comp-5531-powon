<?php
/**
 * @file media.php
 * The powon media system
 */

/**
 * @class
 * Contact categories - intended to be used as a reflection class with Powon_Entity_Link
 */
class Powon_Media_Perm extends Powon_Entity
{
	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as an id
	 */
	function __construct($id=NULL)
	{
		parent::__construct($id, 'permID', 'MediaPerm', array(), FALSE);
	}

	/**
	 * Add permID to references on process
	 */
	function post_process()
	{ $this->reference('dID', $this->dID); }

}


/**
 * Media entity
 */
class Powon_Media extends Powon_Entity { 

	function __construct($id=NULL, &$parent = NULL) 
	{ 
		parent::__construct($id, 'dID', 'Media', array(), TRUE);
		$this->_parent =& $parent;
	} 

	function post_process()
	{
		if (is_null($this->_parent))
			$this->_parent = new Powon_Member($this->creator);
	}

	/**
	 * Register routes
	 */
	static function routing()
	{
		Flight::route('GET /media/@id/edit', array('Powon_Media', 'edit'));
		Flight::route('GET /media/@id/comment', array('Powon_Media', 'comment'));
		Flight::route('POST /media/@id/comment', array('Powon_Media', 'comment'));
		Flight::route('GET /media/@id/comment/@cid/del', array('Powon_Media', 'del_comment'));
		Flight::route('GET /media/@id/del', array('Powon_Media', 'confirm_delete'));
		Flight::route('POST /media/@id/del', array('Powon_Media', 'confirm_delete'));
		Flight::route('GET /media/@id/perm', array('Powon_Media', 'access_control'));
		Flight::route('POST /media/@id/perm', array('Powon_Media', 'access_control'));
		Flight::route('GET /admin/post', array('Powon_Media', 'admin_post'));
		Flight::route('POST /admin/post', array('Powon_Media', 'admin_post'));
	}

    /**
     * Post content
     */
    static function admin_post()
    {  
		$user = Powon::current_user(); 
        if (Powon::access_found(TRUE, $user->is_admin()))
        {   
            $media = new Powon_Media(NULL);
            $media->build_form(array('creator'));

            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {   
                if ($media->_edit->validate())
                {   
                    $data = $media->_edit->data();
                    $data['creator'] = $user->id();
                    $media->save($data);
                    $media->query("INSERT INTO PowonMedia (dID) VALUES (:id)");
                    Powon::redirect(POWON_SUBDIR);
                }
            }


            $media->form();
        }
    }

	/**
	 * Access Control
	 */
	static function access_control($id)
	{
		$media = self::instance($id);
		if (Powon::access_found($media->found(), $media->access('admin')))
		{
			$media->get_perm();
			$cperms = new Powon_Entity_Collection(
				'MemberPerm', 
				"SELECT mcc.name, mp.* FROM `MediaPerm` mp 
				JOIN Media ON mp.dID = Media.dID 
				JOIN Member ON Member.uID = Media.creator 
				JOIN MemberContactCat mcc ON mcc.uID = Member.uID 
				WHERE mcc.permID = mp.permID AND mp.dID = {$media->id()};
", TRUE, 'Powon_Media_Perm', TRUE);
			$gperms = new Powon_Entity_Collection(
				'GroupPerm', 
				"SELECT gr.name, mp.* FROM `MediaPerm` mp 
				JOIN Media ON mp.dID = Media.dID 
				JOIN Member ON Member.uID = Media.creator 
				JOIN GroupMember gm ON gm.uID = Member.uID 
				JOIN `Group` gr ON gr.gID = gm.gID
				WHERE gm.permID = mp.permID AND mp.dID = {$media->id()};
", TRUE, 'Powon_Media_Perm', TRUE);
			$cform = $cperms->form();
			$gform = $gperms->form();
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{   
				$data = array();
				if(isset($_POST["edit-MemberPerm-collection-submit"]))
				{   
					$cform->validate();
					$data = $cform->data();
				}
				else if(isset($_POST["edit-GroupPerm-collection-submit"]))
				{   
					$gform->validate();
					$data = $gform->data();
				}
				if (!empty($data))
				{
					foreach ($data as $submitted)
					{   
						$sql = 'UPDATE MediaPerm SET ';
						foreach ($submitted as $key => $val)
						{   
							if ($key != 'dID' && $key != 'permID' && $key != 'name')
								$sql.= "$key='$val',";
						}
						$sql = rtrim($sql, ',');
						$sql .= " WHERE dID = :id AND permID = " . $submitted['permID'];
						$media->query($sql);
					}
					Powon::redirect();
				}
			}

			$vars = array('title' => $media->format_title() . ' Access Control');
			$vars['forms'] = array(
				array(
					'form' => $cform->generate_table(array('name')),
					'title' => 'Contacts'
				),
				array(
					'form' => $gform->generate_table(array('name')),
					'title' => 'Group Members'
				),
			);
			Powon::render('multi-form.tpl', $vars);
		}
	}


	/**
	 * Edit callback
	 */
	static function edit($id) { parent::edit($id, 'dID', 'Media'); }

	/**
	 * Delete callback
	 */
	static function confirm_delete($id) { parent::confirm_delete($id, 'dID', 'Media'); }

	/**
 	 * Fetch comments
	 */
	function comments()
	{
		if (!isset($this->_linked['MediaComments']))
			$this->link('MediaComments');
		return $this->_linked['MediaComments']->items();
	}

	/**
	 * Delete comments
	 */
	function del_comment($id, $cid)
	{
		$media = self::instance($id);
		if (Powon::access_found($media->found(), ($media->access('admin') || $comment->uID == Powon::current_user()->id())))
		{
			$media->query("DELETE FROM MediaComments WHERE dID = :id AND comID = {$cid}");
			Powon::redirect();
		}
	}

	/**
 	 * Format comments
 	 */
 	function format_comments()
 	{
 		$comments = array();
 		foreach ($this->comments() as $comment)
 		{
 			$new = array(
 				'author' => Powon_Member_Short::instance($comment->uID)->format_title(),
 				'comment' => $comment->text,
 				'del' => FALSE,
 			);
 			if ($this->access('admin') || $comment->uID == Powon::current_user()->id())
 				$new['del'] = $this->format_action("comment/{$comment->comID}/del", 'Remove');
 			$comments[] = $new;
 		}
 		return $comments;
 	}

	/**
	 * Post comment
	 */
	static function comment($id)
	{
		$media = self::instance($id);
		if (Powon::access_found($media->found(), $media->access('edit')))
		{
			$user = Powon::current_user();
			$comment = new Powon_Entity(NULL, 'comID', 'MediaComments');
			$comment->build_form(array('dID', 'comID', 'uID'));

			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if ($comment->_edit->validate())
				{
					$data = $comment->_edit->data();
					$comID = $media->query("SELECT MAX(comID) FROM MediaComments WHERE dID = :id")->fetchColumn();
					$comID++;
					$media->query("INSERT INTO MediaComments (dID, comID, uID, text) VALUES (:id, {$comID}, {$user->id()}, '{$data['text']}')");
					Powon::redirect();
				}
			}
			$comment->form();
		}
	}


	/**
	 * Adjust form
	 */
	function form($tpl='generic-form.tpl', $vars=array()) 
	{
		$this->_edit->exclude(array('creator'));
		parent::form($tpl, $vars); 
	}

	/**
	 * Retrieve permissions
	 */
	function get_perm($id=NULL)
	{
		if (!isset($this->_linked['MediaPerm']))
			$this->link('MediaPerm', NULL, 'Powon_Media_Perm', TRUE);
		if (is_null($id))
			return $this->_linked['MediaPerm']->items();
		else
			return $this->_linked['MediaPerm']->item($id);
	}

	/**
	 * Generate edit links
	 */
	function edit_links($admin=FALSE)
	{
		$links = array();
		if ($admin || $this->access('edit'))
			$links['edit'] = 'Edit';
		if ($admin || $this->access('comment'))
			$links['comment'] = 'Comment';
		if ($admin || $this->access('admin'))
			$links['perm'] = 'Access Control';
		if ($admin || $this->access('del'))
			$links['del'] = 'Remove';
		return $links;
	}

	/**
	 * Save media
	 */
	function save($data)
	{
		parent::save($data);
		$this->_parent = new Powon_Member($data['creator']);
		$pids = $this->_parent->query("SELECT permID FROM MemberPerm WHERE uID = :id");
		while ($row = $pids->fetch(PDO::FETCH_ASSOC))
			$this->query("INSERT INTO MediaPerm (dID, permID) VALUES (:id, {$row['permID']})");
	}

	/**
	 * Retrieve posts by subquery
	 */
	function posts($subquery)
	{
		return Powon_Entity_Collection('Posts', "SELECT post.* FROM Media WHERE dID IN ($subquery)", TRUE, 'Powon_Media');
	}

	/**
	 * Delete media
	 */
	function del()
	{
		$this->query('DELETE FROM MediaPerm WHERE dID = :id');
		$this->query('DELETE FROM MediaComments WHERE dID = :id');
		$this->query('DELETE FROM PowonMedia WHERE dID = :id');
		$this->query('DELETE FROM MemberMedia WHERE dID = :id');
		$this->query('DELETE FROM GroupMedia WHERE dID = :id');
		parent::del();
	}

	/**
	 * Template vars
	 */
	function template_vars()
	{
		return array(
			'title' => $this->format_title(),
			'links' => $this->format_action_list($this->edit_links()),
			'content' => $this->html(),
			'comments' => $this->format_comments(),
		);
	}

	/**
	 * format title
	 */
	function format_title() { return ($this->label) ? $this->label : 'Post'; }

	/**
	 * Override action links
	 */
	function action_link($op) { return "/media/{$this->id()}/{$op}"; }

	/**
	 * labels
	 */
	function label($label) 
	{
		switch (strtolower($label))
		{
			case 'display':
				return 'Content (html tags are allowed)';
				break;

			case 'label':
				return 'Title';
				break;

			default:
				return ucwords($label);
				break;
		}
	}

	/**
	 * Control display of media
	 */
	function html($default=NULL)
	{
		if (is_null($this->display))
			$this->display = $default;
		return $this->display;
	}

	/**
	 * Access check
	 */
	function access($op)
	{
		$user = Powon::current_user();
		$relate = $this->_parent->current_relationship($user);
		$perm = $this->get_perm($relate->id());
		if ($this->_parent->id() == $user->id())
			return TRUE;
		switch ($op)
		{
			case 'json':
				return TRUE;
				break;

			case 'view':
				return ($perm->view || $user->is_admin());
				break;

			case 'edit':
				return ($perm->edit || $user->is_admin());
				break;

			case 'comment':
				return ($perm->comment || $user->is_admin());
				break;

			case 'admin':
			case 'del':
				return $user->is_admin();
				break;

			default:
				return FALSE;
				break;
		}
	}

	static function embed($id) 
	{
		$media = self::instance($id);
		if (Powon::access_found($media->found(), $media->access('view')))
			return $media->html();
	}

}

?>
