<?php
/**
 * @file data.php
 * The powon group system
 */

/**
 * Group specific override
 */
class Powon_Group extends Powon_Entity { 

	function __construct($id=NULL) 
	{ 
		parent::__construct($id, 'gID', 'Group', array(), TRUE);
		$this->_tpl = 'group.tpl';
	} 

	/**
	 * Register routes
	 */
	static function routing()
	{
		Flight::route('GET /json/groups', array('Powon_Group', 'json_groups'));
		Flight::route('GET /group', array('Powon_Group', 'index'));
		Flight::route('GET /group/new', array('Powon_Group', 'create'));
		Flight::route('POST /group/new', array('Powon_Group', 'create'));
		Flight::route('GET /group/@let', array('Powon_Group', 'index'));
		Flight::route('GET /group/@id', array('Powon_Group', 'view'));
		Flight::route('GET /group/@id/index/@let', array('Powon_Group', 'view'));
		Flight::route('GET /group/@id/edit/members', array('Powon_Group', 'edit_members'));
		Flight::route('POST /group/@id/edit/members', array('Powon_Group', 'edit_members'));
		Flight::route('GET /group/@id/edit/confirm', array('Powon_Group', 'confirm_members'));
		Flight::route('GET /group/@id/edit/confirm/@uid', array('Powon_Group', 'group_confirm'));
		Flight::route('GET /group/@id/edit', array('Powon_Group', 'edit'));
		Flight::route('GET /group/@id/join', array('Powon_Group', 'group_join'));
		Flight::route('GET /group/@id/leave', array('Powon_Group', 'group_leave'));
		Flight::route('POST /group/@id/edit', array('Powon_Group', 'edit'));
		Flight::route('GET /group/@id/del', array('Powon_Group', 'confirm_delete'));
		Flight::route('POST /group/@id/del', array('Powon_Group', 'confirm_delete'));
		Flight::route('GET /group/@id/post', array('Powon_Group', 'post'));
		Flight::route('POST /group/@id/post', array('Powon_Group', 'post'));
	}

    /**
     * Post content
     */
    static function post($id)
    {   
        $group = self::instance($id);
        if (Powon::access_found($group->found(), $group->access('edit')))
        {   
            $media = new Powon_Media(NULL);
            $media->build_form(array('creator'));

            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {   
                if ($media->_edit->validate())
                {   
                    $data = $media->_edit->data();
                    $data['creator'] = $group->id();
                    $media->save($data);
                    $group->query("INSERT INTO GroupMedia (gID, dID) VALUES (:id, {$media->id()})");
                    Powon::redirect(POWON_SUBDIR . $group->action_link(''));
                }
            }


            $media->form();
        }
    }

	/**
	 * Join the group
	 */
	static function group_join($id, $member=NULL)
	{
		if (Powon::current_user()->is_user())
		{
			$member = (is_null($member)) ? Powon::current_user()->member() : $member;
			$group = self::instance($id);
			$pid = $member->add_perm();
			$group->query("INSERT INTO GroupMember (uID, gID, permID) VALUES ({$member->id()}, :id, {$pid})");
			Powon::redirect();
		}
	}

	/**
	 * Confirm membership in the group
	 */
	static function group_confirm($id, $mid)
	{
		$group = self::instance($id);
		if (Powon::access_found($group->found(), $group->access('edit')))
		{
			$group->query("UPDATE GroupMember SET confirmed=1 WHERE uID = $mid AND gID = :id");
			Powon::redirect();
		}
	}



	/**
	 * Leave the group
	 */
	static function group_leave($id, $member=NULL)
	{
		if (Powon::current_user()->is_user())
		{
			$member = (is_null($member)) ? Powon::current_user()->id() : $member;
			$group = self::instance($id);
			if (!$group->is_owner($member))
			{
				$group->query("DELETE FROM MemberPerm WHERE permID = (SELECT permID FROM GroupMember WHERE gID = :id AND GroupMember.uID = {$member}) AND MemberPerm.uID = " . $member);
				$group->query("DELETE FROM GroupMember WHERE gID = :id AND uID = " . $member);
			}
			Powon::redirect();
		}
	}



	/**
	 * Create form and handling
	 */
	static function create() { parent::create('Group', 'gID'); }

	/**
	 * Perform some extra operations when a new Group is saved
	 *
	 * @param array $data
	 *	Array of data to save
	 */
	function save($data=NULL)
	{	
		$new = ($this->id() == NULL);
		parent::save($data);
		if ($new)
		{
			// Create permid and group member entry for the creator of the group
			$user = Powon::current_user();
			$new_perm = $user->member()->add_perm();
			$this->query("INSERT INTO GroupMember (uID, gID, role, confirmed, permID) VALUES ({$user->uID}, :id, 'owner', 1, {$new_perm})");
		}
	}

	/**
	 * Edit members
	 */
	static function edit_members($id)
	{
		$group = self::instance($id);
		if (Powon::access_found($group->found(), $group->access('edit')))
		{
			$member = new Powon_Member(NULL);
			$member->build_form(array('pwd', 'midname', 'surname', 'joined', 'status', 'priv', 'avatar'));
			
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$member->_edit->validate();
				$sql = Powon::qry("SELECT uID FROM Member WHERE email = :email AND name = :name AND dob = :dob LIMIT 1",
                    array(
                        ':email' => $member->_edit->data('email'),
                        ':name' => $member->_edit->data('name'),
                        ':dob' => $member->_edit->data('dob')->format('Y-m-d'),
                    ));
				if ($row = $sql->fetch(PDO::FETCH_ASSOC))
				{
					$member = new Powon_Member($row['uID']);
					$new_perm = $member->add_perm();
					$group->query("INSERT INTO GroupMember (uID, gID, confirmed, permID) VALUES ({$member->uID}, :id, 1, {$new_perm})");
				}

			}
			$group->grouped_members()->display_table(
				array('uID', 'status', 'priv', 'avatar', 'joined'),
				'Manage ' . $group->format_title() . ' Members',
				array(
					'entity' => array(
						'' => 'Go', 
						'leave/' . $group->id() => 'Remove',
					),
				),
				array(),
				'generic-table.tpl',
				array(
					'submenu' => $group->submenu(),
					'addform' => $member->_edit->generate()
				)
			);
		}
	}

	/**
	 * Confirm members
	 */
	static function confirm_members($id)
	{
		$group = self::instance($id);
		if (Powon::access_found($group->found(), $group->access('edit')))
		{
			$group->grouped_members(FALSE, 0)->display_table(
				array('uID', 'status', 'priv', 'avatar', 'joined'),
				'Manage ' . $group->format_title() . ' Members',
				array(
					'entity' => array(
						'leave/' . $group->id() => 'Remove',
					),
					'parent' => array(
						'edit/confirm/'  => 'Confirm',
					),
				),
				array(),
				'generic-table.tpl',
				array('submenu' => $group->submenu())
			);
		}
	}



	/**
	 * Properly delete a member
	 *
	 * Media created by the member, as well as invoices to the member are
	 * conserved and must be deleted individually.
	 */
	function del()
	{
		$this->query("DELETE FROM MemberPerm mp WHERE uID IN (SELECT uID FROM GroupMember gm WHERE gID=:id AND gm.uID = mp.uID AND gm.permID = mp.permID);");	
		$this->query("DELETE FROM GroupMember WHERE gID = :id");
		$this->query("DELETE FROM GroupInterest WHERE gID = :id");
		$this->query("DELETE FROM GroupMedia WHERE gID = :id");
		$this->query("DELETE FROM GroupMessage WHERE to = :id");
		parent::del();
	}

	/**
	 * Deletion
	 */
	static function confirm_delete($id) { parent::confirm_delete($id, 'uID', 'Member'); }

	/**
	 * Generate links for the index
	 */
	static function index_links($subdir='/group/')
	{
		$ilinks = array();
		$letters = Powon::qry("SELECT DISTINCT LEFT(name, 1) letter FROM `Group`");
		while ($row = $letters->fetch(PDO::FETCH_ASSOC))
			$ilinks[$row['letter']] = array(
				'href' => POWON_SUBDIR . $subdir . $row['letter'],
				'label' => strtoupper($row['letter']),
				'class' => 'index-link',
			);
		return $ilinks;
	}

	/**
	 * Edit a group
	 */
	static function edit($id)
	{
		parent::edit($id, 'gID', 'Group');
	}

	/**
	 * Edit form display override
	 */
	function form($template='generic-form.tpl', $vars = array())
	{   
		$vars['title'] = $this->format_title();
		if ($this->id())
		{
			$vars['title'] .= ' (' . $this->format_action('del', 'Remove') . ')';
			$vars['submenu'] = $this->submenu();
		}
		parent::form($template, $vars);
	}


	/**
	 * Describe an group and list its members
	 */
	static function view($id, $let=FALSE)
	{
		if (!is_numeric($id))
			return TRUE;
		$group = self::instance($id);
		if (Powon::access_found($group->found(), $group->access('view')))
		{
			if ($let)
				$group->grouped_members($let);
			Powon::render('group.tpl', $group->template_vars());
		}
	}

	/**
	 * Retrieve grouped members
	 */
	function grouped_members($let=FALSE, $confirmed=1)
	{
		Powon::load('powon/member');
		if ($let)
			$this->add_link('Member', new Powon_Entity_Collection('Member', 'SELECT mem.* FROM Member mem NATURAL JOIN GroupMember WHERE mem.name LIKE "' . $let . '%" AND confirmed=' . $confirmed . ' AND gID = ' . $this->id(), TRUE, 'Powon_Member_Short'));		
		if (!isset($this->_linked['Member']))
			$this->add_link('Member', new Powon_Entity_Collection('Member', 'SELECT mem.* FROM Member mem NATURAL JOIN GroupMember WHERE confirmed=' .$confirmed.' AND gID = ' . $this->id(), TRUE, 'Powon_Member_Short'));		
		return $this->_linked['Member'];
	}

	function submenu()
	{
		return $this->format_action_list(array(
			'edit' => 'Edit',
			'edit/members' => 'Manage Members',
			'edit/confirm' => 'Confirm Members'
		));
	}

    /**
     * Return posts
     */
    function posts()
    {
        if (!isset($this->_linked['Media']))
            $this->link('Media', 'GroupMedia', 'Powon_Media');
        return $this->_linked['Media'];
    }

    /**
     * Format posts for display
     */
    function format_posts()
    {
        $posts = array();
        foreach ($this->posts()->items() as $post)
        {
            if ($post->access('view'))
                $posts[] = $post->template_vars();
        }
        return $posts;
    }

	/**
	 * Template variables
	 */
	function template_vars()
	{
		$sq = 'SELECT uID FROM GroupMember WHERE gID = ' . $this->id();
		return array(
			'title' => $this->format_title(),
			'submenu' => $this->format_join_action().$this->format_post_action(), 
			'avatar' => $this->avatar()->html(),
			'desc' => $this->description,
			'posts' => ($this->is_member(Powon::current_user()->id())) ? $this->format_posts() : array(),
			'edit' => ($this->access('edit')) ? '(' . $this->format_action('edit') . ')' : '',
			'members' => $this->grouped_members()->format_grid_items(),
			'index_links' => Powon_Member::index_links(Powon::current_user(), $sq, $this->action_link('index/')),
		);
	}

	/**
	 * List all groups
	 */
	static function index($let=FALSE) {
		if (is_numeric($let))
			return TRUE;
		$user=Powon::current_user();
		if (Powon::access_found(TRUE, $user->is_user()))
		{
			if ($let)
			{
				$let = Powon::dbsafe($let);
				$qry = "SELECT * FROM `Group` WHERE name LIKE '{$let}%' ORDER BY name";
			}
			else
				$qry = "SELECT * FROM `Group` ORDER BY name";
			$groups = new Powon_Entity_Collection('Group', $qry, TRUE, 'Powon_Group');
			$vars = array(
				'index_links' => self::index_links(),
				'submenu' => "<a href='" .POWON_SUBDIR . "/group/new'>Create New Group</a>",
			);
			$groups->display_grid('Groups', 'index-grid.tpl', $vars);
		}
	}

	/**
	 * Output a join link
	 */
	function format_join_action()
	{
		$link = NULL;
		$user = Powon::current_user();
		if (!$this->is_owner($user->id()))
		{
			if ($this->is_member($user->id()))
				$link = $this->format_action('leave');
			else
				$link = $this->format_action('join');
		}
		return $link;
	}

	/**
	 * Output a post link
	 */
	function format_post_action()
	{
		$user = Powon::current_user();
		$link = NULL;
		if ($this->is_member($user->id()))
			$link = $this->format_action('post');
		return $link;
	}

	/**
	 * Format title
	 */
	function format_title() { return ucwords($this->name); }

	/**
	 * action link generation
	 */
	function action_link($op='') { return "/group/{$this->id()}/$op"; }

	/**
 	 * Check if a member is a group member
	 */
	function is_member($mid)
	{
		$mid = (is_a($mid, 'Powon_Member')) ? $mid->id() : $mid;
		return ($this->query("SELECT COUNT(*) FROM GroupMember WHERE uID = {$mid} AND gID = :id")->fetchColumn() > 0);
	}

	/**
 	 * Check if a member is the group owner
	 */
	function is_owner($mid)
	{
		$mid = (is_a($mid, 'Powon_Member')) ? $mid->id() : $mid;
		return ($this->query("SELECT COUNT(*) FROM GroupMember WHERE uID = {$mid} AND gID = :id AND role = 'owner'")->fetchColumn() > 0);
	}

	/**
	 * Access
	 */
	function access($op)
	{
		$access = FALSE;
		$user = Powon::current_user();
		if ($op=='json' || $op=='view')
		{
			$access = $user->is_user();
		}
		else if ($op=='edit' || $op=='admin')
		{
			$access = $user->is_user() && ($user->is_admin() || $this->is_owner($user->id()));
		}
		return $access;
	}

	/**
	 * json access to groups
	 */
	static function json_groups()
	{
		parent::json_list_by('name');
	}

}

?>
