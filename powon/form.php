<?php
/**
 * @file form.php
 * Form generation and validation code
 */

/**
 * @class
 * Represent a form
 */
class Powon_Form
{
	private $_fields;
	private $_buttons;
	private $_data;
	private $_errors;
	private $id;
	private $class;
	private $submit;
	private $reset;
	private $action;

	/**
	 * Prepare a form
	 *
	 * @param string $id
	 *   Unique value for form id attribute
	 * @param string $submit
	 *   Submit button text
	 * @param boolean $reset
	 *   Provide a reset button if true
	 * @param string $action
	 *   Action if any
	 * @param array $classes
	 *   Classes to attribute to the form
	 */
	function __construct($id, $submit, $reset=FALSE, $action='', $classes=array('powon-form')) 
	{
		$this->id = $id;
		$this->class = $classes;
		$this->submit = $submit;
		$this->reset = $reset;
		if ($action)
			$this->action = POWON_SUBDIR . $action;
		$this->_fields = array();
		$this->_buttons = array();
		$this->_data = array();
		$this->_errors = array();

	}

	/**
	 * Exclude the following fields from the form
	 */
	function exclude($excluded)
	{
		foreach ($excluded as $field)
		{
			unset($this->_fields[$field]);
			unset($this->_data[$field]);
		}
	}

	/**
	 * Retrieve a field definition
	 */
	function element($id) { return $this->_fields[$id]; }

	/**
	 * Change this form into a subform
	 */
	function to_subform($key_id, $subform_keys)
	{
		$key_str = "";
		foreach ($subform_keys as $key => $val)
		{
			$key_str .= "$key-$val-";
			$this->add('hidden', $key, $val);
		}
		$key_id = (is_null($key_id)) ? rtrim($key_str, '-') : $key_id;
		foreach ($this->_fields as $id => &$field)
		{
			$field['container'] = $key_id;
			$this->_data[$key_id][$id] = $this->_data[$id];
			unset($this->_data[$id]);
		}
		$this->id = $key_id;
		return $this;
	}

	/**
	 * Generate a table form
	 *
	 * @retval string
	 *   An html form
	 */
	function generate_table($disabled=array())
	{
		$output = "<form action=\"{$this->action}\" method=\"post\" class=\"" . implode(' ', $this->class) . "\" id=\"{$this->id}\">\n";
		$table_suffix = '';
		$table_rows = '';
		$table_header = '<tr>';

		$output .= $this->generate_token();
		$output .= $this->generate_errors();
		$output .= "<table>\n";
		foreach ($this->_fields as $field)
			if ($field['type'] == 'fieldset')
			{
				if ($table_header == '<tr>')
					foreach ($field['fields'] as $subfield)
						$table_header .= "<th class=\"form-{$field['type']}\">" . $subfield['label'] . '</th>';
				$table_rows .= $this->build_table_row($field, $disabled);
			}
			else
				$table_suffix .= $this->build($field);
		$table_header .= '</tr>';
		$output .= $table_header;
		$output .= $table_rows;
		$output .= "</table>\n";
		$output .= $table_suffix;
		$output .= $this->generate_buttons();
		$output .= "</form>\n";

		return $output;
	}



	/**
	 * Generate the form
	 *
	 * @retval string
	 *   An html form
	 */
	function generate()
	{
		$form = "<form action=\"{$this->action}\" method=\"post\" class=\"" . implode(' ', $this->class) . "\" id=\"{$this->id}\">\n";

		$form .= $this->generate_token();
		$form .= $this->generate_errors();
		$form .= $this->generate_elements();
		$form .= $this->generate_buttons();
		$form .= "</form>\n";

		return $form;
	}

	/**
	 * Generate form buttons
	 *
	 * TODO re-enable reset
	 */
	private function generate_buttons()
	{
		$buttons = "<div class=\"powon-form-line powon-form-buttons\"><input name=\"{$this->id}-submit\" type=\"submit\" value=\"{$this->submit}\">\n";
		if ($this->reset && 1==0)
			$buttons .= "<input name=\"{$this->id}-reset\" type=\"button\" value=\"Reset\">\n";
		$buttons .= implode($this->_buttons);
		$buttons .= "</div>\n";
		return $buttons;
	}

	/**
	 * Generate a form token
	 */
	private function generate_token()
	{
		// Add a token
		$token = self::hash(mt_rand());
		$input = "<input type=\"hidden\" name=\"{$this->id}-token\" id=\"{$this->id}-token\" value=\"$token\">\n";
		$_SESSION["{$this->id}-token"] = $token;
		return $input;
	}


	/**
	 * Generate the form elements
	 *
	 * @retval string
	 *   html form elements
	 */
	private function generate_elements()
	{
		$form = "";

		foreach ($this->_fields as $data)
			$form .= $this->build($data) . "\n";

		return $form;
	}

	/**
	 * Generate the form errors
	 *
	 * @retval string
	 *   An html list item
	 */
	private function generate_errors()
	{
		$errors = "";

		if (!empty($this->_errors))
		{
			$errors .= "<ul class=\"powon-form-errors\">\n";
				foreach($this->_errors as $err)
					$errors .= "<li>$err</li>\n";
			$errors .= "<ul>\n";
		}

		return $errors;
	}



	/**
	 * Delete a form element
	 *
	 * @param string $id
	 *   Unique value of the form element
	 */
	function del($id)
	{
		unset($this->_fields[$id]);
		unset($this->_data[$id]);
	}

	/**
	 * Build a form element
	 */
	function build(&$element, $disabled=FALSE)
	{
		$labeled = (is_null($element['label'])) ? "" : "<label for=\"{$element['id']}\">{$element['label']}</label>";
		$default = (isset($element['container'])) ? $this->_data[$element['container']][$element['id']] : $this->_data[$element['id']];
		$html_id = (isset($element['container'])) ? $element['container'] . "[{$element['id']}]" : $element['id'];
		switch ($element['type'])
		{
			case 'date':
				$element['html'] = self::date_input($html_id, $default, $element['label']);
				break;

			case 'select':
				$element['html'] = $labeled . self::select_input($html_id, $element['allowed'], $default);
				break;	

			case 'num':
				$element['html'] = $labeled . self::num_input($html_id, $default);
				break;

			case 'email':
				$element['html'] = $labeled . self::default_input($html_id, $default, 'text', 'email');
				break;

			case 'pwd':
				$element['html'] = $labeled . self::default_input($html_id, $default, 'password');
				break;

			case 'fieldset':
				$element['html'] = $this->build_fieldset($element);
				break;

			case 'hidden':
				$element['html'] = $labeled . self::default_input($html_id, $default, 'hidden');
				break;

			case 'checkbox':
				$element['html'] = $labeled . self::checkbox_input($html_id, $element, $default);
				break;

			case 'autocomplete':
				$element['html'] = '<div class="ui-widget">' . $labeled . self::default_input($html_id, $default) . '</div>';
				break;

			case 'textarea':
				$element['html'] = '<div class="form-row-wrapper">' . $labeled . self::textarea_input($html_id, $default, $element) . '</div>';
				break;

			default:
				if ($disabled)
					$element['html'] = $labeled . self::default_input($html_id, $default, 'hidden', 'disabled-input', TRUE);
				else
					$element['html'] = $labeled . self::default_input($html_id, $default);
				break;
		}
		return $element['html'];
	}

	/**
	 * Add a form element
	 *
	 * @param string $type
	 *   Type of element to add
	 * @param string $id
	 *   Unique value to use for name and id attributes
	 * @param $def
	 *   Appropriate default value or a form object for fieldsets
	 * @param array $allowed
	 *   Array of allowed values
	 */
	function add($type, $id, $def=NULL, $label=NULL, $allowed=array())
	{
		$new = array('type' => $type, 'id' => $id, 'label' => $label, 'allowed' => $allowed);
		switch ($type)
		{
			case 'fieldset':
				$new['id'] = $def->id;
				$id = $def->id;
				$new['fields'] = $def->_fields;
				$def = $def->_data[$id];
				$new['filter'] = FILTER_CALLBACK;
				$new['callback'] = array($this, 'valid_fieldset');
				$new['array'] = TRUE;
				break;

			case 'checkbox':
				$new['filter'] = FILTER_CALLBACK;
				$new['callback'] = array('Powon_Form', 'valid_checkbox');
				$new['array'] = FALSE;
				break;

			case 'date':
				$new['filter'] = FILTER_CALLBACK;
				$new['array'] = TRUE;
				$new['callback'] = array('Powon_Form', 'valid_date');
				break;

			case 'select':
				$new['filter'] = FILTER_SANITIZE_STRING;
				break;	

			case 'num':
				$new['filter'] = FILTER_VALIDATE_FLOAT;
				break;

			case 'email':
				$new['filter'] = FILTER_CALLBACK;
				$new['array'] = FALSE;
				$new['callback'] = array('Powon_Form', 'valid_email');
				break;

			case 'pwd':
				$new['filter'] = FILTER_CALLBACK;
				$new['array'] = FALSE;
				$new['callback'] = array('Powon_Form', 'hash');
				break;

			default:
				$new['filter'] = FILTER_SANITIZE_STRING;
				break;
		}
		if ($type == 'button' || $type == 'submit')
		{
			$this->_buttons[$id] = "<input name=\"{$id}\" type=\"{$type}\" value=\"{$label}\">\n";
		}
		else
		{
			$this->_fields[$id] = $new;
			$this->_data[$id] = $def;
		}
	}

	/**
	 * Validate form token
	 */
	function valid_token($hash)
	{
		if (isset($_SESSION["{$this->id}-token"]) && self::auth($hash, $_SESSION["{$this->id}-token"]))
			return $hash;
		else
			return FALSE;
	}

	/**
	 * Fetch posted results and validate them, reading them into _data
	 *
	 * @retval boolean
	 *   True if all values validated successfully
	 */
	function validate()
	{
		// Reset errors on a new validate
		$this->_errors = array();
	
		if (!filter_input(INPUT_POST, "{$this->id}-token", FILTER_CALLBACK, array('options'=>array($this, 'valid_token'))))
		{
			$this->errors['form'] = "Invalid data";
		}
		else
		{
			foreach ($this->_fields as $label => $field)
			{
				
				$check = $this->filter_field($field, $label);
				if ($check === FALSE)
					$this->_errors[$label] = "Invalid $label";
				else
					$this->_data[$label] = (is_null($this->_data[$label]) && !$check) ? NULL : $check;
			}

		}
		return (empty($this->_errors));
	}

	/**
	 * Run the filter for a given field
	 */
	private function filter_field($field, $label)
	{
		$check = FALSE;
		if ($field['filter'] == FILTER_CALLBACK)
		{
			if ($field['array'])
				$check = call_user_func($field['callback'], $_POST[$label], $label);
			else
				$check = filter_input(INPUT_POST, $label, FILTER_CALLBACK, array('options'=>$field['callback']));
		}
		else
		{
			$check = filter_input(INPUT_POST, $label, $field['filter']);
		}

		return $check;
	}

	/**
	 * Run the filter for a given field on the given value
	 */
	private function filter_field_val($value, $field)
	{
		$check = FALSE;
		if ($field['filter'] == FILTER_CALLBACK)
		{
			if ($field['array'])
				if ($field['type'] == 'fieldset')
					throw new Exception("Could not filter fieldset in fieldset for {$field['id']}");
				else
					$check = call_user_func($field['callback'], $value, $field['label']);
			else
				$check = filter_var($value, FILTER_CALLBACK, array('options'=>$field['callback']));
		}
		else
		{
			$check = filter_var($value, $field['filter']);
		}

		return $check;
	}



	/**
	 * Externally set an error
	 */
	function error($err)
	{
		$this->_errors[] = $err;
	}

	/**
	 * Get the data
	 */
	function data($index=NULL) 
	{
		if (is_null($index))
			return $this->_data;
		else
			return $this->_data[$index];
	}

	/**
	 * Fieldset element as table row
	 */
	function build_table_row($container, $disabled=array())
	{
	 	$output = "<tr><fieldset class=\"powon-form-table-row\" id=\"{$container['id']}\">\n";

		foreach ($container['fields'] as $id => $data)
		{
			$data['label'] = NULL;
			$value = $this->_data[$container['id']][$id];
			if (in_array($data['id'], $disabled))
				$output .= "<td class='form-{$data['type']}'>" . ((is_null($value)) ? 'Default' : $value) . $this->build($data, TRUE) . '</td>';
			else
				$output .= "<td class='form-{$data['type']}'>" . $this->build($data) . '</td>';
		}

	 	$output .= "</fieldset></tr>";
	 	return $output;
	}



	/**
	 * Fieldset element
	 */
	function build_fieldset($container)
	{
	 	$output = "<fieldset class=\"powon-form-fieldset\" id=\"{$container['id']}\">\n";
	 	if (!is_null($container['label']))
	 		$output .= "<legend>{$container['label']}</legend>";

		foreach ($container['fields'] as $data)
			$output .= $this->build($data);

	 	$output .= "</fieldset>";
	 	return $output;
	}

	/**
	 * Numeric input element
	 *
	 * @param string $id
	 *   Value for the id and name attributes
	 * @param string $def
	 *   Default value of element
	 * @param string $classes
	 *   Value to use for class of element
	 * @retval string
	 *   HTML numeric input element
	 */
	static function num_input($id, $def=NULL, $classes='', $min=NULL, $max=NULL)
	{
		$output = "<input type=\"number\" name=\"$id\" id=\"$id\" class=\"$classes\"";
		if (!is_null($min))
			$output .= " min=\"$min\"";
		if (!is_null($max))
			$output .= " min=\"$max\"";
		if (!is_null($def))
			$output .= " value=\"$def\"";
		$output .= '>';
		return $output;
	}

	/**
	 * Checkbox input element
	 *
	 * @param string $id
	 *   Value for the id and name attributes
	 * @param string $element
	 *   Element to render
	 * @param string $def
	 *   Default value of element
	 * @retval string
	 *   HTML numeric input element
	 */
	static function checkbox_input($id, $element, $def=NULL)
	{
		$def = (is_null($def)) ? 0 : $def;
		$output = "<input type=\"checkbox\" name=\"$id\" id=\"$id\"";
		$output .= " value=\"$def\"";
		if ($def)
			$output .= ' checked="checked"';
		$output .= '>';
		return $output;
	}

	/**
	 * Textarea input element
	 *
	 * @param string $id
	 *   Value for the id and name attributes
	 * @param string $def
	 *   Default value of element
	 * @retval string
	 *   HTML text input element
	 */
	static function textarea_input($id, $def=NULL, $element=NULL)
	{
		$output = "<textarea name=\"$id\" id=\"$id\" class=\"textarea-$id\">";
		if (!is_null($def))
			$output .= $def;
		$output .= '</textarea>';
		return $output;
	}

	/**
	 * Default input element
	 *
	 * @param string $id
	 *   Value for the id and name attributes
	 * @param string $def
	 *   Default value of element
	 * @param string $type
	 *   Type to use if not text
	 * @param string $classes
	 *   Value to use for class of element
	 * @retval string
	 *   HTML text input element
	 */
	static function default_input($id, $def=NULL, $type='text', $classes='', $disable=FALSE)
	{
		$output = "<input type=\"$type\" name=\"$id\" id=\"$id\" class=\"$classes\"";
		if (!is_null($def))
			$output .= " value=\"$def\"";
		if ($disable)
			$output .= ' disabled="disabled"';
		$output .= '>';
		return $output;
	}

	/**
	 * Select input element
	 * @param string $id
	 *   Value for the id and name attributes
	 * @param array $allowed
	 *   Array of allowed values
	 * @param string $def
	 *   Default value of element
	 * @param string $classes
	 *   Value to use for class of element
	 * @retval string
	 *   HTML select input element
	 */
	static function select_input($id, $allowed, $def=NULL, $classes='')
	{
		$def = (is_null($def)) ? reset(array_keys($allowed)) : $def;
		$output = "\t<select name=\"$id\" id=\"$id\" class=\"$classes\">\n";
		foreach ($allowed as $val => $opt)
		{
			$output .= "\t\t<option value=\"{$val}\"";
			if ($val == $def)
				$output .=" selected=\"selected\"";
			$output .= ">{$opt}</option>\n";
		}
		$output .= "\t</select>\n";

		return $output;
	}

	/**

	 * Validate an email address
	 *
	 * @param string $email
	 *   Email address to validate
	 * @param boolean $force
	 *   Force will make sure strict validation fails if the mail address itself could not be verified
	 * @return
	 *   the valid email address or false if it could not be validated
	 */
	static function valid_email(&$email, $force=FALSE, $strict=POWON_STRICT_VALIDATION)
	{
		static $rxp = "/^([a-zA-Z0-9_\-.+]+)@([a-zA-Z0-9\-]+.[a-zA-Z]+)$/";
		$matches = array();
		$valid = FALSE;
		if (filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match($rxp, $email, $matches))
		{
			if ($strict)
			{
				$mx;
				if (dns_get_mx($matches[2], $mx))
				{
					$connect = FALSE;
					foreach($mx as $host)
					{
						if (!$connect)
						{
							$ip = gethostbyname($host);
							$connect = @fsockopen($ip, 25);
						}
					}
					if ($connect)
					{
						fwrite($connect , "HELO {$mx[0]}\r\n");
						$mx_helo = fgets($connect, 1024);

						fwrite($connect , "MAIL FROM: " . POWON_SYSTEM_EMAIL . "\r\n");
						$mx_from = fgets($connect, 1024);

						fwrite($connect , "RCPT TO: {$email}\r\n");
						$mx_to = fgets($connect, 1024);

						if (substr($mx_to, 0, 3) == 250)
							$valid = TRUE;
					}
					else // Since we could not connect, only be false if we force email validation 
						$valid = !$force;
				}
				else
					$valid = FALSE;
			}
			else
				$valid = TRUE;
		}
		return ($valid) ? $email : $valid;
	}

	/**
	 * Return a formatted date element with the provided default values for inclusion in a form
	 *
	 * @param string $id
	 *   Value of id and name attributes
	 * @param object $def
	 *   Datetime object to derive default values
	 * @param string $legend
	 *   Content of the legend element
	 * @param boolean $time
	 *   Display time fields as well (not yet implemented)
	 * @retval string
	 *   Formatted date input element
	 */
	 static function date_input($id=NULL, $def=NULL, $legend=NULL, $time=FALSE)
	 {
	 	$def = (is_null($def)) ? new DateTime() : $def;

		$id = (is_null($id)) ? "date" : "$id";

	 	$output = "<fieldset class=\"powon-form-date\" id=\"$id\">\n";
	 	if (!is_null($legend))
	 		$output .= "<legend>$legend</legend>";

		// The year
		$output .= self::num_input("{$id}[y]", $def->format("Y"), 'date-year', 1900, date("Y"));

		// The month
		$refdate = new DateTime("2010-01-01");
		$onemonth = new DateInterval("P1M");
		$months = array();
		for ($i=1; $i<13; $i++)
		{
			$months[$i] = $refdate->format("M");
			$refdate->add($onemonth);
		}
		$output .= self::select_input("{$id}[m]", $months, $def->format("n"), 'date-month');

		// The day
		$output .= self::num_input("{$id}[d]", $def->format("j"), 'date-day', 1, 31);

		$output .= "</fieldset>\n";
		return $output;
	 }

	/**
	 * Validate a number
	 *
	 * @param $val
	 *   A numeric value
	 * @param $min
	 *   Minimum value of the number
	 * @param $max
	 *   Maximum value of the number
	 * @param $filter
	 *   Filter to use, defaults to float
	 * @return
	 *   Sanitized number or false if it could not be validated
	 */
	static function valid_num(&$val, $min, $max, $filter=FILTER_VALIDATE_FLOAT)
	{
	 	if (filter_var($val, $filter) && $val >= $min && $val <= $max)
	 		return $val;
	 	else
	 		return FALSE;
	}

	/**
	 * Alias of valid_num
	 */
	static function valid_int(&$val, $min, $max)
	{
		return self::valid_num($val, $min, $max, FILTER_VALIDATE_INT);
	}

	/**
	 * Validate a fieldset
	 */
	function valid_fieldset($data, $label)
	{
		$return = $this->_data[$label];
		$elements = $this->_fields[$label]['fields'];
		foreach ($return as $key => $submitted) 
		{
			if(isset($data[$key]))
				$submitted = $this->filter_field_val($data[$key], $elements[$key]);
			else if ($elements[$key]['type'] == 'checkbox')
				$submitted = '0';
			$return[$key] = $submitted;
		}
		return $return;
	}

	/**
	 * Validate a checkbo
	 */
	static function valid_checkbox($data)
	{
		$data = filter_var($data, FILTER_VALIDATE_INT);
		if ($data == 0 || $data == 1)
			return '1';
	}

	 /**
	  * Validate a date element
	  *
	  * @param array $data
	  *   An array of values from a date element
	  * @return
	  *   Sanitized date or false if it could not be validated
	  */
	static function valid_date($data, $label=NULL)
	{
	 	$check = -sizeof($data);
	 	$time = array('h'=>0, 'i'=>0, 's'=>0);
	 	if ($data['y'] = self::valid_int($data['y'], 1900, Date('Y')))
	 		$check += 1;
	 	if ($data['m'] = self::valid_int($data['m'], 1, 12))
	 		$check += 1;
	 	if ($data['d'] = self::valid_int($data['d'], 1, 31))
	 		$check += 1;
	 	if (isset($data['h']) && (self::valid_int($data['h'], 0, 24) || $data['h'] === 0))
	 	{
	 		$check += 1;
	 		$time['h'] = $data['h'];
	 		unset($data['h']);
	 	}
	 	if (isset($data['i']) && (self::valid_int($data['i'], 0, 59) || $data['i'] === 0))
	 	{
	 		$check += 1;
	 		$time['i'] = $data['i'];
	 		unset($data['i']);
	 	}
	 	if (isset($data['s']) && (self::valid_int($data['s'], 0, 59) || $data['s'] === 0))
	 	{
	 		$check += 1;
	 		$time['s'] = $data['s'];
	 		unset($data['s']);
	 	}
	 	if ($check===0)
	 		return new DateTime(sprintf('%04d-%02d-%02dT%02d:%02d:%02d', $data['y'], $data['m'], $data['d'], $time['h'], $time['i'], $time['s']));
	 	else
	 		return FALSE;
	}

	/**
	 * Alias of valid_date
	 */
	static function valid_timestamp($data)
	{
		return self::valid_date($data, TRUE);
	}

    /**
     * Hashing function
     *
     * @param string $pass
     *   cleartext password to be hashed
     * @return
     *   the hashed string or false if it could not be validated
     */
    static function hash($pass)
    {
		if (filter_var($pass, FILTER_SANITIZE_STRING))
		{
        	$conf = powon_conf();
        	if (CRYPT_BLOWFISH == 1)
            	$pass = crypt($pass, $conf->blowfish_salt);
        	else if (CRYPT_MD5 == 1)
            	$pass = crypt($pass, $conf->md5_salt);
        	return $pass;
		}
		else
		{
			return FALSE;
		}
    }

    /**
     * Constant time authentication function
     *
     * @param string $hash
     *   hashed string to authenticate
	 * @param string $verit
	 *   hashed string to verify against
     * @retval boolean
     *   true if the authentication was successful
     */
    static function auth($hash, $verit)
    {
        $hxs = $hash ^ $verit;
        $result = strlen($hash) ^ strlen($verit);
        for ($i = strlen($hxs) - 1; $i >= 0; $i--)
        {
            $result += $hxs[$i];
        }
        return ($result == 0);
    }

    /**
     * Generates a simple add form with an autocomplete widget
     */
    static function add_form_instance($id, $json_call)
    {
    	$form = new static("$id-form", 'Add', FALSE, '', array('powon-form', 'powon-add-form'));
    	$form->add('autocomplete', $id);
    	$form->inline_script = "\n" . '$(document).ready(function() {' . "\n\t" . '$(\'#' . $id . '\').autocomplete({source: "' . POWON_SUBDIR.$json_call . '",minLength: 2});' . "\n" . '});' . "\n";
    	return $form;
    }
}
?>
