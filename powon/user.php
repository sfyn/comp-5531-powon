<?php
/**
 * @file user.php
 * The powon user management system
 */

/**
 * @class
 * A generic object for entities in our data model
 */
class Powon_User extends Powon_Entity
{
	static private $current = NULL;

	/**
	 * Register routes
	 */
	static function routing()
	{
		Flight::route('GET /login', array('Powon_User', 'login'));	
		Flight::route('POST /login', array('Powon_User', 'login'));	
		Flight::route('GET /logout', array('Powon_User', 'logout'));	
	}

	/**
	 * Retrieve current user
	 */
	static function current_user()
	{
		if (is_null(self::$current))
			return new static();
		else
			return self::$current;
	}

	/**
	 * Constructor
	 *
	 * @param $id
	 *   Appropriate key value, null for anonymous
	 * @param string $key
	 *   Either email or uID
	 */
	function __construct ($id=NULL, $key=NULL, $force=FALSE)
	{
		if ($force || is_null(self::$current))
		{
			if (is_null($id) && isset($_SESSION['powon_user']))
			{
				$key = 'uID';
				$id = $_SESSION['powon_user'];
			}

			if (!is_null($id))
			{
				parent::__construct($id, $key, 'Member', FALSE, FALSE);
				$this->fetch();
			}
			else
			{
				$this->_id = 0;
			}
			self::$current = $this;
		}
		else if (!is_null(self::$current))
			throw new Exception("Current user exists.", E_WARNING);
	}

	/**
	 * Check this is a real user
	 */
	function is_user()
	{
		return ($this->id()!=0);
	}

	/**
	 * Check this is an admin user
	 */
	function is_admin()
	{
		return ($this->priv == 'admin');
	}

	/**
	 * Retrieve the member object for the user
	 */
	function member()
	{
		static $mem;
		if (!isset($mem))
		{
			Powon::load('powon/member');
			$mem = new Powon_Member($this->id());
		}
		return $mem;
	}

    /**
     * Authenticate the user
     *
     * @param string $hash
     *   hashed password just submitted
     * @retval boolean
     *   true if the authentication was successful
     */
    function auth($hash)
    {
    	return (Powon_Form::auth($hash, $this->pwd));
    }

	/**
	 * Login form generation
	 *
	 * @retval object
	 *   Powon_Formn object
	 */
	static function login_form()
	{
		static $form;
		if (!isset($form))
		{
			$form = new Powon_Form('powon-login-form', 'Login', FALSE, '/login');
			$form->add('email', 'powon-login-mail', '', 'Email');
			$form->add('pwd', 'powon-login-pwd', '', 'Password');
		}
		return $form;
	}

	/**
	 * Login
	 */
	static function login()
	{
		$tplvars = array('login_form'=>NULL, 'login_status'=>NULL);
		if (!isset($_SESSION['powon_user']))
		{
			$form = self::login_form();
			$fail = !$form->validate();
			if (!$fail)
			{
				var_dump($form->data());
				$user = new Powon_User($form->data('powon-login-mail'), 'email', TRUE);
				var_dump($user);
				if ($user->auth($form->data('powon-login-pwd')) && $user->status == 'active')
				{
					$_SESSION['powon_user'] = $user->uID;
					Powon::redirect($_SERVER['HTTP_REFERER'], POWON_SUBDIR . '/login');
					$tplvars['login_status'] = "Logged in!";
				}
				else
					$fail = TRUE;
			}

			if ($fail)
			{
				unset($user);
				$tplvars['login_status'] = "Invalid login credentials";
				$tplvars['login_form'] = $form->generate();
			}
		}
		else
		{
			$tplvars['login_status'] = "Already logged in!";
		}
		Powon::render('login.tpl', $tplvars, FALSE);
	}

	/**
	 * Logout
	 */
	static function logout()
	{
		$tplvars = array('login_form'=>self::login_form()->generate(), 'login_status'=>NULL);
		if (isset($_SESSION['powon_user']))
		{
			unset($_SESSION['powon_user']);
			self::$current = NULL;
			session_destroy();
			if (isset($_SERVER['HTTP_REFERER']))
			{
				header("Location: {$_SERVER['HTTP_REFERER']}");
				exit();
			}
			$tplvars['login_status'] = "Logged out!";
		}
		else
		{
			$tplvars['login_status'] = "Already logged out!";
		}
		// Suppress the login form on the logout page
		$tplvars['login_form'] = NULL;
		Powon::render('login.tpl', $tplvars);
	}


	/**
 	 * Override fetch method for users
	 */
	protected function fetch($cols = array('uID', 'email', 'pwd', 'status', 'priv', 'avatar'), $edit=FALSE)
	{
		parent::fetch($cols, $edit);
	}
}

?>
