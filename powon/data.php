<?php
/**
 * @file data.php
 * The powon data system
 */

/**
 * @class
 * A generic link between entities
 */
class Powon_Entity_Collection
{
	protected $_name;
	protected $_ents;
	protected $_class;
	protected $_editable;
	protected $_query;
	protected $_order;
	protected $_parent;

	/**
	 * Constructor
	 *
	 * @param string $name
	 *   The table name corresponding to the entity linked to
	 * @param string $class
	 *   A class to use for constructing linked entities
	 * @param boolean $fetch
	 *   Whether or not to fetch on construction
	 */
	function __construct ($name, $query=NULL, $fetch=TRUE, $class='Powon_Entity', $editable=FALSE, $order=FALSE)
	{
		$this->_name = Powon::dbsafe($name);
		$this->_ents = array();
		$this->_order = $order;
		$this->_class = ($class == 'Powon_Entity') ? NULL : new ReflectionClass($class);
		$this->_editable = $editable;
		$this->_query = (is_null($query)) ? $this->make_query() : $query;
		if ($fetch)
			$this->fetch_all();
	}

	/**
	 * Generate the query for fetching data
	 */
	function make_query($cols = array())
	{
		$fields = (empty($cols)) ? '*' : 'ref.' . implode(', ref.', $cols);
		$query = "SELECT {$fields} FROM `{$this->_name}` ref";
		if ($this->_order)
			$query .= " ORDER BY ref.{$this->_order}";
		return $query;	
	}

	/**
	 * Query wrapper function
	 */
	function query() { return Powon::qry($this->_query); }

	/**
	 * Fetch all data lines
	 *
	 * @param array $cols
	 *   Column names to fetch, an empty array is treated as all columns
	 */
	function fetch_all($cols = array())
	{
		$req = $this->query();
	
		$this->_ents = array();

		while ($row = $req->fetch(PDO::FETCH_NUM))
		{
			if (is_null($this->_class))
				$new_ent = new Powon_Entity(NULL, NULL, $this->_name, array(), FALSE, $this->_editable);
			else
				$new_ent = $this->_class->newInstance(NULL);
			if ($new_ent->process($row, $req, $cols, $this->_editable))
			{
				$this->ent_process($new_ent);
				if (!is_null($new_ent->id()))
					$this->_ents[$new_ent->id()] = $new_ent;
				else
					$this->_ents[] = $new_ent;
			}
		}
	}

	/**
	 * Things to do during entity processing
	 */
	protected function ent_process(&$ent)
	{
		$ent->alias($this->_name, 'ref');
	}

	/**
	 * Get an array of values for the given attribute
	 */
	function get_values($key)
	{
		$values = array();
		foreach ($this->_ents as $id => $ent)
			if (!is_null($ent->__get($key)))
				$values[$id] = $ent->__get($key);
		return $values;
	}

	/**
	 * Return the entities
	 */
	function items() { return $this->_ents; }

	/**
	 * Return an entity by index
	 */
	function item($i) { return $this->_ents[$i]; }

	/**
	 * Check if we may edit the contents
	 */
	function is_editable() { return $this->_editable; }

	/**
	 * Generate a form for editing all contained elements
	 */
	function form()
	{
		$form = new Powon_Form("edit-{$this->_name}-collection", 'Save');
		foreach ($this->_ents as $ent)
		{
			$form->add('fieldset', '', $ent->subform($ent->key() . '-' . $ent->id()));
		}
		return $form;
	}

	/**
	 * Get a formatted list of entities
	 */
	function format_items($ops=array(), $before='<span class="entity-item-name">', $after='</span>', $link=TRUE)
	{
		$items = array();
		foreach ($this->_ents as $ent)
		{
			$data = $ent->format_data();
			$label = $ent->format_title();
			$label = ($link) ? $ent->format_action('', $label) : $label;
			$line = "{$before}{$label}{$after}";
			if (isset($ops['user']))
			{
				$user = Powon::current_user()->member();
				foreach ($ops['user'] as $op => $label)
					$line .= $user->format_action($op . $ent->id(), $label);
			}
			$items[] = $line;
		}
		return $items;
	}

	/**
	 * Display a grid of entities
	 */
	function display_grid($title, $tpl='index-grid.tpl', $vars=array('index_links' => FALSE))
	{
		$vars['grid_items'] = $this->format_grid_items();
		$vars['title'] = $title;
		Powon::render($tpl, $vars);
	}

	/**
	 * Format grid items
	 */
	function format_grid_items()
	{
		$grid_items = array();
		foreach ($this->_ents as $key => $entity)
		{
			$grid_items[] = array(
				'img' => $entity->avatar()->html(),
				'link' => POWON_SUBDIR . $entity->action_link(''),
				'label' => $entity->format_title()
			);
		}
		return $grid_items;
	}


	/**
	 * Display a table of entities
	 *
	 * This is intended to be used to provide lists of entities and to provide
	 * administrative controls for these entities. See invocations in Powon_Member
	 * for some usage examples.
	 *
	 * The $ops array should have an 'entity' key and a 'user' optional key.
	 * Entity operations will be generated from the entity's perspective, while
	 m
	 * user operations will be generated using the current user and appending the
	 * entity id to the provided url.
	 *
	 * @param array $exclude_cols
	 *   Array of column names to exclude from display
	 * @param string $title
	 *   Title of the page on which the table is displayed
	 * @param array ops
	 *   Associative array(s) of urls => human readable labels
	 * @param array override
	 *   Function to use to override display of a column
	 */
	function display_table($exclude_cols = array(), $title=NULL, $ops=array(), $override=array(), $template='generic-table.tpl', $template_vars = array(), $render=TRUE)
	{
		$template_vars['title'] = (is_null($title)) ? $this->_name : $title;
		$template_vars['table_header'] = array();
		foreach ($this->_ents as $key => $entity)
		{
			$template_vars['table_rows'][$key] = $entity->format_data($override);
			if (empty($template_vars['table_header']))
				foreach ($template_vars['table_rows'][$key] as $label => $value)
					$template_vars['table_header'][$label] = $entity->label($label);
			if (!empty($ops))
			{
				$template_vars['table_rows'][$key]['ops'] = "";
				if (isset($ops['parent']))
				{
					foreach ($ops['parent'] as $op => $label)
						$template_vars['table_rows'][$key]['ops'] .= $this->parent()->format_action($op . $entity->id(), $label);
				}
				if (isset($ops['entity']))
					$template_vars['table_rows'][$key]['ops'] .= $entity->format_action_list($ops['entity']);
				if (isset($ops['user']))
				{
					$user = Powon::current_user()->member();
					foreach ($ops['user'] as $op => $label)
						$template_vars['table_rows'][$key]['ops'] .= $user->format_action($op . $entity->id(), $label);
				}
			}
		}
		$template_vars['table_header'] = array_diff_key($template_vars['table_header'], array_flip($exclude_cols));
		if (!empty($ops))
			$template_vars['table_header']['ops'] = "";
		if ($render)
			Powon::render($template, $template_vars);
		else
			return $template_vars;
	}

	/**
	 * Retrieve or set the parent object
	 */
	function parent(&$ref = NULL) 
	{
		if ($ref)
			$this->_parent =& $ref;
		return $this->_parent; 
	}
}



/**
 * @class
 * A generic link between entities
 */
class Powon_Entity_Link extends Powon_Entity_Collection
{
	protected $_lookup;

	/**
	 * Constructor
	 *
	 * @param string $name
	 *   The table name corresponding to the entity linked to
	 * @param string $parent
	 *   Reference to the parent entity
	 * @param string $lookup
	 *   A lookup table if needed
	 * @param string $class
	 *   A class to use for constructing linked entities
	 * @param boolean $fetch
	 *   Whether or not to fetch on construction
	 */
	function __construct ($name, &$parent, $lookup=NULL, $fetch=TRUE, $class='Powon_Entity', $editable=FALSE, $order=FALSE)
	{
		$this->_lookup = (is_null($lookup)) ? $lookup : Powon::dbsafe($lookup);
		$this->_parent =& $parent;
		parent::__construct($name, NULL, FALSE, $class, $editable, $order);
		if ($fetch)
			$this->fetch_all();
	}

	/**
	 * Generate the query for fetching data
	 */
	function make_query($cols = array())
	{
		$fields = (empty($cols)) ? '*' : 'ref.' . implode(', ref.', $cols);
		$query = "SELECT {$fields} FROM `{$this->_name}` ref";
		if (!is_null($this->_lookup))
			$query .= " NATURAL JOIN {$this->_lookup} lk WHERE lk.{$this->_parent->key()} = :id";
		else
			$query .= " WHERE ref.{$this->_parent->key()} = :id";
		if ($this->_order)
			$query .= " ORDER BY ref.{$this->_order}";
		return $query;	
	}

	/**
	 * Query wrapper function
	 */
	function query() { return $this->_parent->query($this->_query); }

	/**
	 * Things to do during entity processing
	 */
	protected function ent_process(&$ent)
	{
		$ent->reference($this->_parent->key(), $this->_parent->id());
		$ent->alias($this->_parent->name(), 'par');
		$ent->alias($this->_name, 'ref');
		$ent->alias($this->_lookup, 'lk');
	}

	/**
	 * Delete a link
	 */
	function del_item($lid)
	{
		$item = $this->item($lid);
		Powon::qry("DELETE FROM {$this->_lookup} WHERE {$this->_parent->key()} = :id AND {$item->key()} = :lid",
			array(
				':id' => $this->_parent->id(),
				':lid' => $item->id(),
			));
	}


}

/**
 * @class
 * A generic link between entities
 */
class Powon_Entity_Join extends Powon_Entity_Link
{
	/**
	 * Constructor
	 *
	 * @param string $name
	 *   The table name corresponding to the entity linked to
	 * @param string $parent
	 *   Reference to the parent entity
	 * @param string $lookup
	 *   A lookup table if needed
	 * @param string $class
	 *   A class to use for constructing linked entities
	 * @param boolean $fetch
	 *   Whether or not to fetch on construction
	 */
	function __construct ($name, &$parent, $lookup=NULL, $fetch=TRUE, $class='Powon_Entity', $editable=FALSE, $order=FALSE)
	{
		parent::__construct($name, $parent, $lookup, $fetch, $class, $editable, $order=FALSE);
	}

	/**
	 * Generate the query for fetching data
	 */
	function make_query($cols = array())
	{
		$fields = (empty($cols)) ? 'ref.*' : 'ref.' . implode(', ref.', $cols);
		$query = "SELECT {$fields} FROM `{$this->_name}` ref";
		if (!is_null($this->_lookup))
			$query .= " NATURAL JOIN {$this->_lookup} lk";
		$query .= " NATURAL JOIN `{$this->_parent->name()}` par WHERE par.{$this->_parent->key()} = :id";
		if ($this->_order)
			$query .= " ORDER BY ref.{$this->_order}";
		return $query;	
	}
}

/**
 * @class
 * A generic object for entities in our data model
 */
class Powon_Entity
{
	private $_name;
	private $_key;
	private $_id;
	private $_data;
	private $_avatar;
	protected $_meta;
	protected $_refs;
	protected $_tables;
	protected $_edit;
	protected $_linked;
	protected $_tpl;
	protected $_parent;

	/**
	 * Register routes
	 */
	static function routing()
	{
		Flight::route('GET /data/@name/@key/@id', array('Powon_Entity', 'dispatch'));
		Flight::route('GET /data/@name/@key/@id/@op', array('Powon_Entity', 'dispatch'));
		Flight::route('POST /data/@name/@key/@id/@op', array('Powon_Entity', 'dispatch'));
	}

	/**
	 * Dispatch a request
	 *
	 * @param string $n
	 *   Entity name
	 * @param string $k
	 *   Entity key
	 * @param $id
	 *   Unique id
	 * @param string $op
	 *   Operation to perform
	 */
	static function dispatch($n, $k, $id, $op='view')
	{
		// Filter argumemts
		$op = strtolower(filter_var($op, FILTER_SANITIZE_STRING));
		self::filter($id, $k, $n);
		if ($_SERVER['REQUEST_METHOD'] == 'GET')
		{
			// Dispatch to appropriate function
			switch ($op)
			{
				case 'edit':
					self::edit($id, $k, $n);
					break;

				case 'del':
					self::confirm_delete($id, $k, $n);
					break;

				case 'view':
				default:
					self::view($id, $k, $n);
					break;
			}
		}
		else if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			switch ($op)
			{
				case 'edit':
					self::edit($id, $k, $n, TRUE);
					break;

				case 'del':
					self::confirm_delete($id, $k, $n);
					break;

				default:
					Flight::notFound();
					break;
			}
		}
	}

	/**
	 * Try to find an id
	 */
	function find_id($key, $val)
	{
		$key = Powon::dbsafe($key);
		$sql = Powon::qry("SELECT {$this->_key} FROM `{$this->_name}` WHERE {$key} = :val LIMIT 1;", array(':val' => $val));
		$this->_id = $sql->fetchColumn();
		return $this->_id;
	}

	/**
	 * Determine if an instance of such an entity can be found
	 */
	static function in_db($key, $val, $table=FALSE)
	{
		if (!$table)
			$table = $ent = self::instance(NULL)->name();
		$key = Powon::dbsafe($key);
		$sql = Powon::qry("SELECT COUNT($key) FROM {$table} WHERE {$key} = :val;", array(':val' => $val));
		return ($sql->fetchColumn() > 0);
	}

	/**
	 * Display an entity attribute via json
	 */
	static function json_attr($id, $attr)
	{
		$ent = self::instance($id);
		if (Powon::access_found($ent->found(), $ent->access('json')))
		{
			Flight::json($ent->__get($attr));
		}
	}

	/**
	 * Display a list of values for the entity, on a given key, jsonized
	 * 
	 * Intended to be called from a child. Uses the get var term to filter results.
	 */
	static function json_list_by($key)
	{
		$ent = self::instance(NULL);
		if (Powon::access_found(TRUE, $ent->access('json')))
		{
			$key = Powon::dbsafe($key);
			$term = strtolower(filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING));
			$sql = Powon::qry("SELECT DISTINCT {$key} FROM `{$ent->name()}` WHERE {$key} LIKE \"{$term}%\";");
			Flight::json($sql->fetchAll(PDO::FETCH_COLUMN));
		}
	}

	/**
	 * Display an entity
	 *
	 * @param $id
	 *   Unique id
	 * @param string $k
	 *   Entity key
	 * @param string $n
	 *   Entity name
	 */
	static function view($id, $k, $n)
	{
		if ($id == 'new')
			return TRUE;
		else
		{
			$entity = self::instance($id, $k, $n);
			$entity->display();
		}
	}

	/**
	 * Edit an entity
	 *
	 * @param $id
	 *   Unique id
	 * @param string $k
	 *   Entity key
	 * @param string $n
	 *   Entity name
	 * @param $update
	 *   Before displaying the form, process the posted data
	 */
	static function edit($id, $k, $n, $update=FALSE)
	{   
		$entity = self::instance($id, $k, ucwords($n));
		if (Powon::access_found($entity->found(), $entity->access('edit')))
		{
			if ($update || $_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if ($entity->_edit->validate())
					$entity->save($entity->_edit->data());
				if ($dest = filter_input(INPUT_GET, 'redirect', FILTER_SANITIZE_URL))
					Powon::redirect($dest);
			}
			$entity->form();
		}
	}

	/**
	 * Create page for an entity
	 */
	static function create($n, $k=NULL)
	{
		$ent = self::instance(NULL, $k, ucwords($n));
		if (Powon::access_found($n, $ent->access('new')))
		{
			$ent->build_form(array('avatar'));
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$ent->_edit->validate();
				$ent->save();
				Powon::redirect(POWON_SUBDIR . $ent->action_link(''));
			}
			else
				$ent->form();
		}
	}

	/**
	 * Build the metas for the entity
	 */
	function build_meta()
	{
		if (empty($this->_meta))
		{
			$meta = Powon::qry("SELECT * FROM `{$this->_name}` LIMIT 0");
			for ($i=0; $m = $meta->getColumnMeta($i); $i++)
				$this->_meta[$m['name']] = $m;
		}
	}

	/**
	 * Build a form for the entity from scratch
	 */
	function build_form($exclude = array())
	{
		$this->_edit = new Powon_Form("edit-{$this->_name}-{$this->_id}", 'Save', TRUE);
		$this->build_meta();
		$defaults = Powon::qry('SELECT COLUMN_NAME, COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "' . powon_conf()->dbname . '" AND TABLE_NAME = "' . $this->_name . '"');
		while ($def = $defaults->fetch(PDO::FETCH_ASSOC))
		{
			$m = $this->_meta[$def['COLUMN_NAME']];
			if ($m['name'] != $this->_key && !in_array($m['name'], $exclude))
				$this->add_input($m, $def['COLUMN_DEFAULT']);
		}
	}

	/**
	 * Save the entity
	 *
	 * @param array $data
	 *   Array of data to save
	 */
	function save($data=NULL)
	{
		if (isset($this->_edit) && !is_null($this->_edit))
		{
			if (is_null($data))
				$data = $this->_edit->data();
			if (is_null($this->_id))
			{
				$this->build_meta();
				Powon::qry($this->build_insert_query($data, TRUE));
				$this->_id = Powon::db_last_id("`{$this->_name}`.`{$this->_key}`");
			}
			else
			{
				$this->query($this->build_update_query($data));
			}
			$this->fetch(array(), TRUE);
		}
	}

	/**
	 * Confirm and delete the entity
	 */
	static function confirm_delete($id, $k, $n)
	{
		$entity = self::instance($id, $k, ucwords($n));
		if (Powon::access_found($entity->found(), ($entity->access('del') || $entity->access('admin'))))
		{
			$form = Powon::confirm_form(NULL, NULL);
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$form->validate();
				$yes = is_null(filter_input(INPUT_POST, 'no', FILTER_SANITIZE_STRING));
				if ($yes)
					$entity->del();
				Powon::redirect();
			}
			else
			{
				$vars = array(
					'title' => "Delete " . $entity->format_title() . "?",
					'form' => $form->generate(),
				);
				Powon::render('generic-form.tpl', $vars);
			}
		}
	}

	/**
	 * Delete the entity
	 */
	function del() 
	{
		$this->query("DELETE FROM `{$this->_name}` WHERE {$this->_key} = :id"); 
	}

	/**
	 * Format an attribute for display
	 */
	function format_attr($label, $val, $wrap='') 
	{
		//if ($this->_meta[$label]['name'] == 'avatar')
		//	return Powon_Media::embed
		return $this->stringify($label, $val, $wrap); 
	}

	/**
	 * Convert values to approriate strings using column metas
	 */
	protected function stringify($label, $val, $wrap='')
	{
		$output = $val;
		switch($this->_meta[$label]['native_type'])
		{
			case 'DATE':
				$output = $wrap . $val->format("Y-m-d") . $wrap;
				break;

			case 'TIMESTAMP':
				$output = $wrap . $val->format("Y-m-d G:i:s") . $wrap;
				break;

			default:
				switch ($this->_meta[$label])
				{
				}
				$output = (is_null($val)) ? 'NULL' : $wrap.$val.$wrap;
				break;
				
		}
		return $output;
	}

	/**
	 * Entity factory function
	 *
	 * @param $id
	 *   Unique id
	 * @param string $k
	 *   Entity key
	 * @param string $n
	 *   Entity name
	 * @retval object
	 *   A powon entity
	 */
	static function instance($id, $k=NULL, $n=NULL)
	{
		self::filter($id, $k, $n);
		$instance = NULL;
		if (get_called_class() == 'Powon_Entity')
			$instance = new static($id, $k, $n, array(), FALSE);
		else
			$instance = new static($id);
		if (!is_null($id))
			$instance->fetch();
		return $instance;
	}

	/**
	 * Sanitize parameters used to load the entity
	 *
	 * @param $id
	 *   Unique id
	 * @param string $k
	 *   Entity key
	 * @param string $n
	 *   Entity name
	 */
	static function filter(&$id, &$k, &$n)
	{
		$n = ucwords(filter_var($n, FILTER_SANITIZE_STRING));
		$k = filter_var($k, FILTER_SANITIZE_STRING);
		$id = (is_null($id)) ? NULL : filter_var($id, FILTER_SANITIZE_STRING);
	}

	/**
	 * Add a ready made collection to the linked array
	 */
	function add_link($link, $coll)
	{
		$this->_linked[$link] = $coll;
		$this->_linked[$link]->parent($this);
	}

	/**
	 * Link another entity type to this one
	 *
	 * @param string $link
	 *   The table name to link to
	 * @param string $lookup
	 *   If provided, this will use an intermediary lookup table
	 */
	function link($link, $lookup = NULL, $class='Powon_Entity', $editable=FALSE, $order=FALSE)
	{
		$this->_linked[$link] = new Powon_Entity_Link($link, $this, $lookup, TRUE, $class, $editable, $order);
	}

	/**
	 * Link another entity type to this one joining on the entity table
	 *
	 * @param string $link
	 *   The table name to link to
	 * @param string $lookup
	 *   If provided, this will use an intermediary lookup table
	 */
	function join($link, $lookup = NULL, $class='Powon_Entity', $editable=FALSE, $order=FALSE)
	{
		$this->_linked[$link] = new Powon_Entity_Join($link, $this, $lookup, TRUE, $class, $editable, $order);
	}

	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as a unique id
	 * @param string $key
	 *   Which field to use as a key
	 * @param string $name
	 *   The table name corresponding to the entity
	 * @param array $data
	 *   Existing data to use for the entity
	 * @param boolean $fetch
	 *   Whether or not to fetch on construction
	 * @param boolean $editable
	 *   Construct an edit form for the object too
	 */
	function __construct ($id, $key, $name, $data = array(), $fetch=TRUE, $editable=TRUE)
	{
		Powon::load('powon/form');
		$this->_meta = array();
		if ($editable)
			$this->_edit = new Powon_Form("edit-{$name}-{$id}", 'Save', TRUE);
		else
			$this->_edit = NULL;
		// Escape the name and key because they will be used directly in the query
		$this->_name = Powon::dbsafe($name);
		$this->_key = Powon::dbsafe($key);
		// The id will be parametrized by PDO
		$this->_id = $id;
		$this->_data = $data;
		$this->_linked = array();
		$this->_tpl = 'entity.tpl';
		if ($fetch)
			$this->fetch(array(), $editable);
	}

	/**
	 * Can the entity be found
	 */
	function found()
	{
		if (is_null($this->_id))
			return FALSE;
		else
			return ($this->query("SELECT COUNT({$this->_key}) FROM `{$this->_name}` WHERE {$this->_id} = :id")->fetchColumn() > 0);
	}

	/**
	 * Default access check
	 *
	 * @param string $op
	 *   Operation being requested
	 * @retval boolean
	 *   Whether the current user can access the entity
	 */
	function access($op='view')
	{
		$user = Powon::current_user();
		return $user->is_admin();
	}

	/**
	 * Determine if the entity is editable
	 */
	function is_editable() { return (!is_null($this->_edit)); }
 
	/**
	 * Retrieve the id
	 */
	function id() { return $this->_id; }

	/**
	 * Retrieve the name
	 */
	function name() { return $this->_name; }

	/**
	 * Retrieve the key
	 */
	function key() { return $this->_key; }

	/**
	 * Retrieve the column names in data
	 */
	function cols() { return array_keys($this->_data); }

	/**
	 * Retrieve the data
	 */
	function data() { return $this->_data; }

	/**
	 * Add a reference
	 */
	function reference($k, $v) { $this->_refs[$k] = $v; }

	/**
	 * Add a table alias
	 */
	function alias($table, $alias) { $this->_tables[$alias] = $table; }

	/**
	 * Generate labels for elements
	 *
	 * Override this in child classes to specify labels which differ from database
	 * column headers.
	 */
	function label($val) { return $val; }

	/**
	 * Fetch data members
	 *
	 * @param array $cols
	 *   Column names to fetch, an empty array is treated as all columns
	 * @param boolean $edit
	 *   Construct an edit form for the object too
	 */
	protected function fetch($cols = array(), $edit=TRUE)
	{
		$req = $this->query($this->build_select_query($cols));
		$this->process($req->fetch(PDO::FETCH_NUM), $req, $cols, $edit);
	}

	/**
	 * Default fetch query
	 */
	protected function build_select_query($cols=array()) 
	{
		$fields = (empty($cols)) ? '*' : implode(',', $cols);
		return "SELECT {$fields} FROM `{$this->_name}` WHERE {$this->_key} = :id"; 
	}

	/**
	 * Default insert query
	 */
	protected function build_insert_query($data, $keep_key=FALSE) 
	{
		if (!$keep_key)
			$data = array_diff_key($data, array($this->_key => NULL));
		$vals = array();
		foreach ($data as $k => $v)
		{
			$v = $this->stringify($k, $v, '"');
			if ($v)
				$vals[$k] = $v;
		}
		$cols = '(`' . implode('`, `', array_keys($vals)) . '`)';
		$vals = '(' . implode(', ', $vals) . ')';
		$query = "INSERT INTO `{$this->_name}` {$cols} VALUES {$vals}";
		return $query;
	}

	/**
	 * Default update query
	 */
	protected function build_update_query($data) 
	{
		$affected_tables = array();
		$set = array();
		foreach ($data as $k => $v)
		{
			if ($k != $this->_key && (is_null($this->_refs) || !in_array($k, array_keys($this->_refs))))
			{
				if (empty($this->_tables))
				{
					$set[$k] = "`$k` = {$this->stringify($k, $v, '"')}";
				}
				else
				{
				$affected_tables[$this->_meta[$k]['table']] = $this->_tables[$this->_meta[$k]['table']];
				$set[$k] = $affected_tables[$this->_meta[$k]['table']] . ".$k = {$this->stringify($k, $v, '"')}";
				}
			}
		}
		$query = "UPDATE `{$this->_name}`";
		foreach ($affected_tables as $table)
			if ($this->_name != $table)
				$query .= " NATURAL JOIN `$table`";
		$query .= " SET " . implode(', ', $set) . " WHERE `{$this->_name}`.{$this->_key} = :id";
		if (isset($this->_refs))
			foreach ($this->_refs as $ref => $val)
				$query .= " AND {$affected_tables[$this->_meta[$ref]['table']]}.{$ref}='{$val}'";
		return $query;
	}

	/**
	 * Query runner
	 *
	 * @param string $query
	 *   Query to run
	 * @return
	 *   Executed PDO request
	 */
	function query($query) { return Powon::qry($query, array(':id' => $this->_id)); }

	/**
	 * Try to set and return the avatar
	 */
	function avatar()
	{
		Powon::load('powon/media');
		if (!isset($this->_avatar))
		{
			$id = ($this->avatar) ? $this->avatar : NULL;
			$this->_avatar = new Powon_Media($this->avatar);
			$this->_avatar->html('<img class="member-avatar" src="' . POWON_SUBDIR . '/powon/logo/head.png">');
		}
		return $this->_avatar;
	}

	/**
	 * Process a row from a PDO request object to regenerate data
	 *
	 * @param array $row
	 *   A row extracted using fetch(PDO::FETCH_NUM)
	 * @param object $req
	 *   The request
	 * @param boolean $cols
	 *   Original column names to fetch
	 * @param boolean $edit
	 *   Construct an edit form for the object too
	 * @retval boolean
	 *   True if an accessible entity was built
	 */
	function process($row, &$req, $cols, $edit)
	{
		$out = FALSE;
		if ($row)
		{
			$this->_data = array();
			foreach($row as $c => $d)
			{
				$meta = $req->getColumnMeta($c);
				$this->_meta[$meta['name']] = $meta;
				$this->transpose($meta, $d);
				if ($this->_id == NULL && $meta['name'] == $this->_key)
					$this->_id = $d;
				if ($edit && $meta['name'] != $this->_key && $meta['name'] != 'avatar')
					$this->add_input($meta, $d);
			}
			$out = TRUE;
			$this->post_process();
		}
		else
		{ // Make the entity refer to nothing
			$this->_id = NULL;
		}

		// Make sure no password field is retained unless it is explicitly requested
		if (isset($this->_data['pwd']) && !in_array('pwd', $cols))
		{
			unset($this->_data['pwd']);
			if ($edit)
				$this->_edit->del("pwd");
		}

		return $out;
	}

	/**
	 * Do things to the entity after processing
	 */
	function post_process() {}

	/**
	 * Add an appropriate input element to the edit form
	 *
	 * @param $meta
	 *   Column meta information
	 * @param $data
	 *   The raw data as extracted from SQL
	 */
	protected function add_input($meta, $data=NULL)
	{
		$label = $this->label($meta['name']);
		switch ($meta['native_type'])
		{
			case 'DATE':
			case 'TIMESTAMP':
				$data = ($data == 'CURRENT_TIMESTAMP') ? NULL : $data;
				$data = (is_a($data, 'DateTime')) ? $data : new DateTime($data);
				$this->_edit->add('date', $meta['name'], $data, $label);
				break;

			case 'TINY':
				if ($meta['len'] == 1)
					$this->_edit->add('checkbox', $meta['name'], $data, $label);
				else
					$this->_edit->add('num', $meta['name'], $data, $label);
				break;

			case 'BLOB':
				$this->_edit->add('textarea', $meta['name'], $data, $label);
				break;
				

			default:
				switch ($meta['name'])
				{
					case 'email':
						$this->_edit->add('email', $meta['name'], $data, $label);
						break;

					case 'pwd':
						$this->_edit->add('pwd', $meta['name'], $data, $label);
						break;

					default:
						$this->_edit->add('text', $meta['name'], $data, $label);
						break;
				}
				break;
		}
	}

	/**
	 * Display edit form for this entity
	 */
	function form($template='generic-form.tpl', $vars = array())
	{
		$vars['title'] = (isset($vars['title'])) ? $vars['title'] : $this->format_title();
		$vars['form'] = $this->_edit->generate();
		Powon::render($template, $vars);
	}

	/**
	 * Generate form elements for this entity for inclusion in another form
	 */
	function subform($key_id=null) 
	{
		$subform_keys = $this->_refs;
		$subform_keys[$this->key()] = $this->id();
		return $this->_edit->to_subform($key_id, $subform_keys); 
	}

	/**
	 * Transpose SQL data types to PHP classes
	 *
	 * @param $meta
	 *   Column meta information
	 * @param $data
	 *   The raw data as extracted from SQL
	 * @retval
	 *   The resulting formatted object, which is also assigned to data
	 */
	protected function transpose($meta, &$data)
	{
		switch ($meta['native_type'])
		{
			case 'DATE':
			case 'TIMESTAMP':
				$data = new DateTime($data);
				break;

			default:
				break;
		}
		$this->_data[$meta['name']] = $data;
		return $data;
	}

	/**
	 * Generate a title
	 *
	 * @retval string
	 *   A title for this entity
	 */
	function format_title($suffix = "") { return "{$this->_name} {$this->_id} {$suffix}"; }

	/**
	 * Generate an action link
	 */
	function format_action($op, $label=NULL, $redirect=TRUE) 
	{	
		$active = parse_url($_SERVER['REQUEST_URI'])['path'];
		$href = POWON_SUBDIR . $this->action_link($op);
		$label = (is_null($label)) ? ucwords($op) : $label;
		if ($active == $href)
			return NULL;
		if ($redirect)
			$href .= Powon::redirect_value();
		return '<a class="powon-entity-action action-' . str_replace(' ', '-', strtolower($label)) . '" href="' . $href . '">' . $label . '</a>'; 
	}

	/**
	 * Generate a series of actions from an array of action links
	 */
	function format_action_list($ops, $redirect=TRUE)
	{
		$actions = "";
		foreach ($ops as $op => $label)
			$actions .= $this->format_action($op, $label, $redirect) . "\n";
		return $actions;
	}

	/**
	 * Format data for display
	 */
	function format_data($override=array())
	{
		$data = array();
		foreach ($this->_data as $label => $value)
		{
			if (in_array($label, array_keys($override)))
				$data[$label] = call_user_func($override[$label], $value, $this);
			else
				$data[$label] = $this->format_attr($label, $value);
		}
		return $data;
	}

	/**
	 * Format and label data for display
	 */
	function format_label_data()
	{
		$data = array();
		foreach ($this->_data as $label => $value)
		{
			$data[$this->label($label)] = $this->format_attr($label, $value);
		}
		return $data;
	}
	

	/**
	 * Register template variables
	 *
	 * @retval array
	 *   key value pairs for assignment to a view
	 */
	function template_vars()
	{
			return array(
			'entity_title' => $this->format_title(),
			'entity_data' => $this->format_label_data(),
		);
	}

	/**
	 * Public access to template vars
	 */
	function embed_template_vars() { return template_vars(); }

	/**
	 * Display the entity using a generic template
	 */
	function display() { Powon::render_if($this->found(), $this->access('view'), $this->_tpl, array($this, 'template_vars')); }

	/**
	 * Provide action links for the entity
	 */
	function action_link($op) { return "/data/{$this->_name}/{$this->_key}/{$this->_id}/{$op}"; }

	/**
	 * Dynamic getter
	 */
	function __get($key)
	{   
		if ($this->_data && array_key_exists($key, $this->_data))
			return $this->_data[$key];
		else
			return NULL;
	}

	/**
	 * Dynamic setter
	 */
	function __set($key, $val)
	{
		$this->_data[$key] = $val;
	}
}

/**
 * Interest specific override
 */
class Powon_Interest extends Powon_Entity { 

	function __construct($id=NULL) 
	{ 
		parent::__construct($id, 'iID', 'Interest', array(), FALSE); 
	} 

	/**
	 * Register routes
	 */
	static function routing()
	{
		Flight::route('GET /json/tags', array('Powon_Interest', 'json_tags'));
		Flight::route('GET /interest', array('Powon_Interest', 'index'));
		Flight::route('GET /interest/@let', array('Powon_Interest', 'index'));
		Flight::route('GET /interest/@id', array('Powon_Interest', 'view'));
		Flight::route('GET /interest/@id/index/@let', array('Powon_Interest', 'view'));
		Flight::route('GET /interest/@id/edit', array('Powon_Interest', 'edit'));
		Flight::route('POST /interest/@id/edit', array('Powon_Interest', 'edit'));
	}

	/**
	 * Generate links for the index
	 */
	static function index_links($subdir='/interest/')
	{
		$ilinks = array();
		$letters = Powon::qry("SELECT DISTINCT LEFT(label, 1) letter FROM Interest");
		while ($row = $letters->fetch(PDO::FETCH_ASSOC))
			$ilinks[$row['letter']] = array(
				'href' => POWON_SUBDIR . $subdir . $row['letter'],
				'label' => strtoupper($row['letter']),
				'class' => 'index-link',
			);
		return $ilinks;
	}

	/**
	 * Edit an interest
	 */
	static function edit($id)
	{
		parent::edit($id, 'iID', 'Interest');
	}

	/**
	 * Describe an interest and list its members
	 */
	static function view($id, $let=FALSE)
	{
		if (!is_numeric($id))
			return TRUE;
		$interest = self::instance($id);
		if (Powon::access_found($interest->found(), $interest->access('view')))
		{
			if ($let)
				$interest->interested_members($let);
			Powon::render('interest.tpl', $interest->template_vars());
		}
	}

	/**
	 * Retrieve interested members
	 */
	function interested_members($let=FALSE)
	{
		Powon::load('powon/member');
		if ($let)
			$this->add_link('Member', new Powon_Entity_Collection('Member', 'SELECT mem.* FROM Member mem NATURAL JOIN MemberInterest WHERE mem.name LIKE "' . $let . '%" AND iID = ' . $this->id(), TRUE, 'Powon_Member_Short'));		
		if (!isset($this->_linked['Member']))
			$this->link('Member', 'MemberInterest', 'Powon_Member_Short');
		return $this->_linked['Member'];
	}

	/**
	 * Template variables
	 */
	function template_vars()
	{
		$sq = 'SELECT uID FROM MemberInterest WHERE iID = ' . $this->id();
		return array(
			'title' => $this->format_title(),
			'avatar' => $this->avatar()->html(),
			'desc' => $this->description,
			'edit' => ($this->access('edit')) ? '(' . $this->format_action('edit') . ')' : '',
			'members' => $this->interested_members()->format_grid_items(),
			'index_links' => Powon_Member::index_links(Powon::current_user(), $sq, $this->action_link('index/')),
		);
	}

	/**
	 * List all interests
	 */
	static function index($let=FALSE) {
		if (is_numeric($let))
			return TRUE;
		$user=Powon::current_user();
		if (Powon::access_found(TRUE, $user->is_user()))
		{
			if ($let)
			{
				$let = Powon::dbsafe($let);
				$qry = "SELECT * FROM Interest WHERE label LIKE '{$let}%' ORDER BY label";
			}
			else
				$qry = "SELECT * FROM Interest ORDER BY label";
			$interests = new Powon_Entity_Collection('Interest', $qry, TRUE, 'Powon_Interest');
			$vars = array('index_links' => self::index_links());
			$interests->display_grid('Interests', 'index-grid.tpl', $vars);
		}
	}

	/**
	 * Format title
	 */
	function format_title() { return ucwords($this->label); }

	/**
	 * action link generation
	 */
	function action_link($op='') { return "/interest/{$this->id()}/$op"; }

	/**
	 * Access
	 */
	function access($op)
	{
		if ($op=='json' || $op=='view')
			return Powon::current_user()->is_user();
		else
			return parent::access($op);
	}

	/**
	 * json access to tags
	 */
	static function json_tags()
	{
		parent::json_list_by('label');
	}

}

?>
