<?php

if (FALSE)
{

	include('conf.php');
	$db = new PDO('mysql:host=' . powon_conf()->dbhost . ';dbname=' . powon_conf()->dbname, powon_conf()->dbuser, powon_conf()->dbpwd);

	$names = array('Ahmed', 'Aliyev', 'Bauer', 'Bogdani', 'Bosu', 'Brown', 'Chakma', 'Chan', 'Chung', 'Cohen', 'Cruz', 'Davis', 'Devi', 'Diaz', 'Dimitrov', 'Doe', 'Dubois', 'Dushku', 'Flores', 'Gagnon', 'Gao', 'Garcia', 'Gonzales', 'Gruber', 'Han', 'Hasan', 'Horvat', 'Ivanov', 'Jensen', 'Johnson', 'Jones', 'Kim', 'Kumar', 'Lee', 'Lopez', 'Mahmud', 'Martin', 'Miller', 'Morales', 'Murphy', 'Ng', 'Nguyen', 'Nielsen', 'Novik', 'Oliveira', 'Papadopoulos', 'Perez', 'Petrov', 'Popa', 'Popescu', 'Popovic', 'Ramos', 'Rodriguez', 'Rossi', 'Roy', 'Saito', 'Santos', 'Schmidt', 'Shin', 'Silva', 'Singh', 'Sheikh', 'Smirnov', 'Smith', 'Sousa', 'Sun', 'Suzuki', 'Tanaka', 'Thomas', 'Tran', 'Tremblay', 'Wagner', 'Wazed', 'Wilson', 'Williams', 'Wong', 'Wu', 'Xu', 'Yang', 'Zhao', 'Zogaj');

	$interests = array('jazz', 'cryptography', 'swing', 'heavy metal', 'pottery', 'horses', 'fantasy', 'science fiction', 'archery', 'astronomy', 'cycling', 'fishing', 'baseball', 'soccer', 'birdwatching', 'knitting', 'crocheting', 'travel', 'astrology', 'poker', 'chess', 'programming', 'gardening', 'environmentalism', 'graffiti', 'swimming', 'golfing', 'gaming', 'rpg', 'tarot', 'dance', 'ballet', 'opera', 'stamps', 'yoga', 'meditation');

	$iid = array();

	$cats = array('NULL', '"Family"', '"Friend"', '"Colleague"');

	for ($i=1; $i<36; $i++) { $iid[] = $i; }

	foreach ($interests as $int)
	{
		$db->query("INSERT INTO Interest (label) VALUES ('{$int}')");
	}

	foreach ($names as $name)
	{
		$email = strtolower($name) . '@example.com';
		$ten_years = 315360000;
		$dob = date("Y-m-d", mt_rand((time() - ($ten_years*7)), (time() - $ten_years)));
		$db->query("INSERT INTO Member (name, email, dob) VALUES ('{$name}', '{$email}', '{$dob}')");
		$uID = $db->lastInsertId();
		$db->query("INSERT INTO Location (label) VALUES ('primary')");
		$lID = $db->lastInsertId();
		$db->query("INSERT INTO MemberLocation VALUES ({$uID}, {$lID}, 0, 1)");
		for ($i=0; $i<4; $i++) 
		{
			$db->query("INSERT INTO MemberPerm (uID, permID) VALUES ({$uID}, {$i})");
			$db->query("INSERT INTO MemberContactCat VALUES ({$uID}, {$i}, {$cats[$i]},{$i})");
		}
		$db->query("INSERT INTO MemberInterest VALUES ({$uID}, " . mt_rand(1,36) . ")");
	}
}
?>
