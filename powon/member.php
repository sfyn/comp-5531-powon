<?php
/**
 * @file member.php
 * Member representation
 */

/**
 * @class
 * member permissions - intended to be used as a reflection class with Powon_Entity_Link
 */
class Powon_Member_Perm extends Powon_Entity
{
	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as an id
	 */
	function __construct($id=NULL)
	{
		parent::__construct($id, 'permID', 'MemberPerm', array(), FALSE);
	}

	/**
 	 * Add uID to references on process
	 */
	function post_process() 
	{ $this->reference('uID', $this->uID); }

	/**
	 * Check if an access is allowed for the given op / item combination
	 *
	 * Note that this is intended to be used within the context of a Member access
	 * check, and not to determine user access to the specific MemberContactCat
	 * instance.
	 */
	function check($perm)
	{
		$check = $this->viewAll; // Allow viewAll to override
		if (!$check)
		{
			$check = $this->__get($perm);
			$check = (is_null($check)) ? TRUE : $check; // Account for the possibility that no such permission exists
		}
		return $check;
	}

}


/**
 * @class
 * Contact categories - intended to be used as a reflection class with Powon_Entity_Link
 */
class Powon_Member_Cat extends Powon_Member_Perm
{
	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as an id
	 */
	function __construct($id=NULL)
	{
		Powon_Entity::__construct($id, 'catID', 'MemberContactCat', array(), FALSE);
	}

	/**
 	 * Add permID to references on process
	 */
	function post_process() 
	{ $this->reference('permID', $this->permID); }

}


/**
 * @class
 * Contact categories - intended to be used as a reflection class with Powon_Entity_Link
 */
class Powon_Message extends Powon_Entity
{
	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as an id
	 */
	function __construct($id=NULL)
	{
		Powon_Entity::__construct($id, 'mID', 'Message', array(), FALSE);
	}

	/**
 	 * Add from to references on process
	 */
	function post_process() 
	{ $this->reference('from', $this->from); }

	/**
 	 * Message routing
	 */
	static function routing()
	{
		Flight::route('GET /message/@id/@uid', array('Powon_Message', 'view'));
		Flight::route('GET /message/@id/@uid/del', array('Powon_Message', 'erase'));
	}

	/**
	 * View a message
	 */
	static function view($id, $mid)
	{
		$id = Powon::dbsafe($id);
		$mid = Powon::dbsafe($mid);
		$user= Powon::current_user();
		$msg = Powon::qry("SELECT mem.name AS `sender`, m.from, m.content, m.mID, m.time FROM Message m NATURAL JOIN MemberMessage mm JOIN Member mem ON mem.uID = m.`from` WHERE m.`from` = {$mid} AND  m.mID = {$id} AND mm.`to` = " . $user->id())->fetch(PDO::FETCH_ASSOC);
		if (Powon::access_found($msg, $user->is_user()))
		{
			$vars = array(
				'title' => substr($msg['content'], 0, 64),
				'message' => $msg,
				'sender' => "<a href='" . POWON_SUBDIR. "/member/{$mid}'>{$msg['sender']}</a>",
				'submenu' => "<a href='" . POWON_SUBDIR . "/inbox'>Back</a>",
			);
			Powon::render('message.tpl', $vars);
		}
		
	}

	/**
	 * Delete a message
	 */
	static function erase($id, $mid)
	{
		$id = Powon::dbsafe($id);
		$mid = Powon::dbsafe($mid);
		$user= Powon::current_user();
		$msg = Powon::qry("SELECT mem.name AS `sender`, m.from, m.content, m.mID, m.time FROM Message m NATURAL JOIN MemberMessage mm JOIN Member mem ON mem.uID = m.`from` WHERE m.`from` = {$mid} AND  m.mID = {$id} AND mm.`to` = " . $user->uID)->fetch(PDO::FETCH_ASSOC);
		if (Powon::access_found($msg, $user->is_user()))
		{
			Powon::qry("DELETE FROM MemberMessage WHERE mID = $id AND `from` = $mid AND `to` = " . $user->uID);
			Powon::qry("DELETE FROM Message WHERE mID = $id AND `from` = $mid");
			Powon::redirect();
		}
		
	}



	/**
	 * Override action links
	 */
	function action_link($op) { return "/message/{$this->id()}/$this->from/{$op}"; }

}



/**
 * @class
 * Link to contacts
 */
class Powon_Contact_Link extends Powon_Entity_Link
{
	/**
	 * Constructor
	 *
	 * @param string $name
	 *   The table name corresponding to the entity linked to
	 * @param string $parent
	 *   Reference to the parent entity
	 * @param string $lookup
	 *   A lookup table if needed
	 * @param boolean $fetch
	 *   Whether or not to fetch on construction
	 */
	function __construct (&$parent, $fetch=TRUE)
	{
		parent::__construct('Member', $parent, 'MemberContact', $fetch, 'Powon_Member_Short', FALSE);
	}

	/**
	 * Generate the query for fetching data
	 */
	function make_query($cols = array())
	{
		$fields = (empty($cols)) ? 'con.*, mc.catID' : 'con.' . implode(', con.', $cols) . ', mc.catID';
		$query = "SELECT {$fields} FROM {$this->_name} con JOIN MemberContact mc ON mc.cID = con.uID JOIN Member par ON par.uID = mc.uID WHERE par.uID = :id";
		return $query;
	}

	/**
	 * Override fetch_all to make sure we use just the cols we want
	 */
	function fetch_all($cols = array('uID', 'email', 'name', 'midname', 'surname', 'dob'))
	{
		parent::fetch_all($cols);
	}

	/**
	 * Delete a link
	 */
	function del_item($lid)
	{
		Powon::qry("DELETE FROM MemberContact WHERE uID = :id AND cID = :lid",
			array(
				':id' => $this->_parent->id(),
				':lid' => $lid,
			));
	}


}

/**
 * @class
 * Shortened version of regular member object
 */
class Powon_Member_Short extends Powon_Member
{
	function __construct($id=NULL)
	{
		Powon_Entity::__construct($id, 'uID', 'Member', array(), FALSE);
		$this->_tpl = 'member.tpl';
	}

	function post_process()
	{
	}
}

/**
 * @class
 * Member object contains member data for display and access
 */
class Powon_Member extends Powon_Entity
{
	protected $_location;
	static $default_cats = array(NULL, 'Family', 'Friend', 'Colleague');

	/**
	 * Constructor
	 *
	 * @param $id
	 *   A value to use as a unique id
	 */
	function __construct($id=NULL)
	{	
		if (is_null($id))
		{
			parent::__construct($id, 'uID', 'Member', array(), FALSE);
		}
		else
		{
			parent::__construct($id, 'uID', 'Member');
			$this->add_link('Location', new Powon_Location_Join($this, 'MemberLocation'));
			$this->add_link('MemberContact', new Powon_Contact_Link($this));
			$this->link('Interest', 'MemberInterest', 'Powon_Interest', FALSE, 'label');
		}
		$this->_tpl = 'member.tpl';
	}

	/**
	 * Try to find the primary location of the user
	 */
	function find_location($force = FALSE)
	{
		if (is_null($this->id()))
			throw new Exception('Could not find location due to unknown member id.', E_WARNING);
		else if (!isset($this->_location) || $force)
			$this->_location = Powon_Location::primary_address($this->id());
		return $this->_location;
	}

	/**
	 * Override process function to run find_location afterwards
	 */
	function post_process()
	{
		$this->find_location(TRUE);
		$this->link('MemberContactCat', 'MemberPerm', 'Powon_Member_Cat', TRUE);
	}

	/**
	 * Register routes
	 */
	static function routing()
	{
		Powon_Message::routing();
		Flight::route('GET /admin/member', array('Powon_Member', 'admin_list'));
		Flight::route('GET /admin/member/index', array('Powon_Member', 'admin_list'));
		Flight::route('GET /admin/member/index/@alpha', array('Powon_Member', 'admin_list'));
		Flight::route('GET /join', array('Powon_Member', 'join_site'));
		Flight::route('POST /join', array('Powon_Member', 'join_site'));
		Flight::route('GET /profile', array('Powon_Member', 'my_profile'));
		Flight::route('GET /member', array('Powon_Member', 'index'));
		Flight::route('GET /member/index', array('Powon_Member', 'index'));
		Flight::route('GET /member/index/@alpha', array('Powon_Member', 'index'));
		Flight::route('GET /member/@id', array('Powon_Member', 'view'));
		Flight::route('GET /member/@id/post', array('Powon_Member', 'post'));
		Flight::route('POST /member/@id/post', array('Powon_Member', 'post'));
		Flight::route('POST /member/@id/msg', array('Powon_Member', 'msg'));
		Flight::route('GET /member/@id/msg', array('Powon_Member', 'msg'));
		Flight::route('GET /inbox', array('Powon_Member', 'inbox'));
		Flight::route('GET /member/@id/leave/@gid', array('Powon_Member', 'leave_group'));
		Flight::route('GET /member/@id/toggle/@val', array('Powon_Member', 'toggle'));
		Flight::route('GET /member/@id/edit', array('Powon_Member', 'edit'));
		Flight::route('POST /member/@id/edit', array('Powon_Member', 'edit'));
		Flight::route('GET /member/@id/del', array('Powon_Member', 'confirm_delete'));
		Flight::route('POST /member/@id/del', array('Powon_Member', 'confirm_delete'));
		Flight::route('GET /member/@id/edit/interest', array('Powon_Member', 'edit_interests'));
		Flight::route('POST /member/@id/edit/interest', array('Powon_Member', 'edit_interests'));
		Flight::route('GET /member/@id/del/interest/@iid', array('Powon_Member', 'del_interest'));
		Flight::route('GET /member/@id/edit/address', array('Powon_Member', 'edit_address'));
		Flight::route('POST /member/@id/edit/address', array('Powon_Member', 'edit_address'));
		Flight::route('GET /member/@id/edit/contact', array('Powon_Member', 'manage_contacts'));
		Flight::route('POST /member/@id/edit/contact/@cid', array('Powon_Member', 'save_contact'));
		Flight::route('POST /member/@id/add/contact/@cid', array('Powon_Member', 'add_contact'));
		Flight::route('GET /member/@id/del/contact/@cid', array('Powon_Member', 'del_contact'));
		Flight::route('GET /member/@id/edit/access', array('Powon_Member', 'edit_access'));
		Flight::route('POST /member/@id/edit/access', array('Powon_Member', 'edit_access'));
		Flight::route('GET /member/new', array('Powon_Member', 'create'));
		Flight::route('POST /member/new', array('Powon_Member', 'create'));
	}

	/**
	 * Return posts
	 */
	function posts()
	{
		if (!isset($this->_linked['Media']))
			$this->link('Media', 'MemberMedia', 'Powon_Media');
		return $this->_linked['Media'];
	}

	/**
	 * Return messages
	 */
	function messages()
	{
		if (!isset($this->_linked['Message']))
		{
			$this->add_link('Message', new Powon_Entity_Collection('Message', "SELECT mem.name AS `sender`, m.from, SUBSTRING(m.content, 1,127) AS content, m.mID, m.time FROM Message m NATURAL JOIN MemberMessage mm JOIN Member mem ON mem.uID = m.`from` WHERE mm.`to` = " . $this->id(), TRUE, 'Powon_Message'));
		}
		return $this->_linked['Message'];
	}

	/**
	 * User inbox
	 */
	static function inbox()
	{
		$user = Powon::current_user();
		if (Powon::access_found($user->found(), $user->is_user()))
		{
			$mem = $user->member();
			$mem->messages()->display_table(
				array('mID', 'from'),
				'My Inbox',
				array(
					'entity' => array(
						'' => 'View',
						'del' => 'Remove',
					)
				),
				array(),
				'generic-table.tpl',
				array(
				)
			);
		}
	}

	/**
	 * Format posts for display
	 */
	function format_posts()
	{
		$posts = array();
		foreach ($this->posts()->items() as $post)
		{
			if ($post->access('view'))
				$posts[] = $post->template_vars();
		}
		return $posts;
	}

	/**
	 * Post content
	 */
	static function post($id)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('edit')))
		{
			$media = new Powon_Media(NULL);
			$media->build_form(array('creator'));

			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if ($media->_edit->validate())
				{
					$data = $media->_edit->data();
					$data['creator'] = $member->id();
					$media->save($data);
					$member->query("INSERT INTO MemberMedia (uID, dID) VALUES (:id, {$media->id()})");
					Powon::redirect(POWON_SUBDIR . $member->action_link(''));
				}
			}


			$media->form('generic-form.tpl', $member->ui_tpl_vars());
		}
	}

	/**
	 * Message a member
	 */
	static function msg($id)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('view')))
		{
			$msg = new Powon_Message(NULL);
			$msg->build_form(array('from', 'time'));

			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if ($msg->_edit->validate())
				{
					$user = Powon::current_user();
					$data = $msg->_edit->data();
					$data['from'] = $user->uID;
					$mid = $user->member()->query("SELECT MAX(mID) FROM Message WHERE `from` = :id")->fetchColumn();
					$data['mID'] = ++$mid;
					$msg->save($data);
					$member->query("INSERT INTO MemberMessage (`to`, `from`, `mID`) VALUES (:id, {$user->uID}, {$msg->id()})");
					Powon::redirect(POWON_SUBDIR . $member->action_link(''));
				}
			}

			$msg->form('generic-form.tpl', array('title'=>'Message '.$member->format_title()));
		}
	}



	/**
	 * Toggle status or privilege
	 */
	static function toggle($id, $val)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('admin')))
		{
			$val = Powon::dbsafe($val);
			$new = FALSE;
			switch ($val)
			{
				case 'status':
					$current = $member->query("SELECT status FROM Member WHERE uID = :id")->fetchColumn();
					$new = ($current == 'active') ? 'inactive' : 'active';
					break;
				case 'priv':
					$current = $member->query("SELECT priv FROM Member WHERE uID = :id")->fetchColumn();
					$new = ($current == 'admin') ? 'member' : 'admin';
					break;
				default:
					break;
			}
			if ($new)
				$member->query("UPDATE Member SET {$val}='{$new}' WHERE uID = :id");
		}
		Powon::redirect();
	}


	/**
	 * Leave a group
	 */
	static function leave_group($uid, $gid)
	{
		Powon::load('powon/group');
		Powon_Group::group_leave($gid, $uid);
	}


	/**
	 * Jump to user profile
	 */
	static function my_profile()
	{
		$user = Powon::current_user();
		if ($user->is_user())
			Powon::redirect(POWON_SUBDIR . '/member/' . $user->uID);
		else
			Powon::not_found();
	}

	/**
	 * Administrative list of members
	 */
	static function admin_list($letter=NULL)
	{
		$user = Powon::current_user();
		if (Powon::access_found(TRUE, $user->is_admin()))
		{
			$fields = array('uID', 'name', 'midname', 'surname', 'dob', 'status', 'priv');
			$members = new Powon_Entity_Collection('Member', self::build_list_query($fields, $user, $letter, TRUE), TRUE, 'Powon_Member_Short');
			$members->display_table(
				array(),
				'Members',
				array(
					'entity' => array(
						'' => 'Go',
						'edit' => 'Edit',
						'toggle/priv' => 'Toggle Admin',
						'toggle/status' => 'Toggle Status',
						'del' => 'Remove',
					), 
				),
				array(), 
				'index-table.tpl', 
				array(
					'index_links'=>self::index_links($user, FALSE, '/admin/member/index/', TRUE)
				)
			);
		}
	}

	/**
	 * Display a list of members
	 */
	static function index($letter=NULL)
	{
		$user = Powon::current_user();
		if (Powon::access_found(TRUE, $user->is_user()))
		{
			$members = new Powon_Entity_Collection('Member', self::build_grid_query($user, $letter), TRUE, 'Powon_Member_Short');
			$members->display_grid('Contacts', 'index-grid.tpl', array('index_links'=>self::index_links($user)));
		}
	}

	/**
	 * Construct index links
	 */
	static function index_links($user, $subquery=FALSE, $subdir='/member/index/', $admin=FALSE)
	{
		$ilinks = array();
		if ($admin)
			$query = "SELECT LEFT(name, 1) letter FROM Member UNION SELECT LEFT(surname, 1) letter FROM Member";
		else if ($subquery)
			$query = "SELECT LEFT(name, 1) letter FROM Member WHERE	 Member.uID IN ({$subquery})";
		else
			$query = "SELECT LEFT(name, 1) letter FROM Member 
				NATURAL JOIN MemberContact WHERE cID={$user->uID} 
					UNION 
				SELECT LEFT(name, 1) letter FROM Member 
				WHERE uID IN (
					SELECT uID FROM MemberContact 
					NATURAL JOIN MemberContactCat 
					NATURAL JOIN MemberPerm 
					WHERE viewFullName=1 AND cID={$user->uID}
				)";
		$req = Powon::qry("SELECT DISTINCT * FROM ({$query}) Letters ORDER BY Letters.letter");
		while ($row = $req->fetch(PDO::FETCH_ASSOC))
			$ilinks[$row['letter']] = array(
				'href' => POWON_SUBDIR . $subdir . $row['letter'],
				'label' => $row['letter'],
				'class' => 'index-link',
			);
		return $ilinks;
	}

	/**
	 * Fetch a list of members
	 */
	static function build_list_query($fields, $user, $letter=NULL, $admin=FALSE)
	{
		$user = Powon::current_user();
		$fields = implode(', ', $fields);
		$query = "SELECT {$fields} FROM Member NATURAL JOIN MemberLocation NATURAL JOIN Location WHERE isPrimary=1";
		$clauses = self::list_query_clauses($user, $letter, $admin);
		if ($clauses)
			$query .= " AND {$clauses}";
		return $query;
	}

	/**
	 * Fetch a list of members for a grid
	 */
	static function build_grid_query($user, $letter=NULL, $admin=FALSE)
	{
		$user = Powon::current_user();
		$query = "SELECT uID, name, midname, surname, avatar FROM Member";
		$clauses = self::list_query_clauses($user, $letter, $admin);
		if ($clauses)
			$query .= " WHERE {$clauses}";
		return $query;
	}



	/**
	 * query clauses for index
	 */
	static function list_query_clauses($user, $letter=NULL, $admin=FALSE)
	{
		$clauses = "";
		if (!$admin && $user->is_user())
			$clauses .= "Member.uID IN (SELECT cID FROM MemberContact WHERE uID = {$user->uID})";
		if (!is_null($letter))
		{   
			if ($clauses)
				$clauses .= " AND ";
			$letter = Powon::dbsafe($letter);
			$clauses .= "(Member.name COLLATE UTF8_GENERAL_CI LIKE '{$letter}%')";
		}
		return $clauses;
	}

	/**
	 * Edit form and handling
	 */
	static function edit($id)
	{   
		$entity = self::instance($id);
		if (Powon::access_found($entity->found(), $entity->access('edit')))
		{   
			$entity->_edit->exclude(array('joined', 'status', 'priv', 'avatar'));
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{   
				if ($entity->_edit->validate())
					$entity->save($entity->_edit->data());
				if ($dest = filter_input(INPUT_GET, 'redirect', FILTER_SANITIZE_URL))
					Powon::redirect($dest);
			}
			$entity->_edit->exclude(array('joined', 'status', 'priv', 'avatar'));
			$entity->form();
		}
	}

	/**
	 * Create form and handling
	 */
	static function create() { parent::create('Member', 'uID'); }

	/**
	 * Join the site
	 */
	static function join_site()
	{
		$user = Powon::current_user();
		if (Powon::access_found(TRUE, !$user->is_user()))
		{   
			$new_mem = self::instance(NULL);
			$new_mem->build_form(array('joined', 'status', 'priv', 'avatar'));
			$ref_mem = self::instance(NULL);
			$ref_mem->build_form(array('pwd', 'midname', 'surname', 'joined', 'status', 'priv', 'avatar'));
			$ref_mem->_edit->add('select', 'contact-category', 1, 'Relationship', array(1=>'Family', 2=>'Friend', 3=>'Colleague'));

			$new_mem->_edit->add('fieldset', 'referer', $ref_mem->_edit->to_subform('referer', array()), 'Referrer');
			
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{   
				$new_mem->_edit->validate();
				$data = $new_mem->_edit->data();
				$sql = Powon::qry("SELECT * FROM Member WHERE email = :email AND name = :name AND dob = :dob LIMIT 1",
					array(
						':email' => $data['referer']['email'],
						':name' => $data['referer']['name'],
						':dob' => $data['referer']['dob']->format('Y-m-d'),
					));
				if ($row = $sql->fetch(PDO::FETCH_NUM))
				{
					$ref_mem->process($row, $sql, array(), FALSE);
					$catID = Powon::dbsafe($data['referer']['contact-category']);
					unset($data['referer']);
					$new_mem->save($data);
					$contact_qry = "INSERT INTO MemberContact VALUES (:uid, :cid, :catID)";
					$contact_params = array(
						':uid' => $ref_mem->id(),
						':cid' => $new_mem->id(),
						':catID' => $catID,
					);
					Powon::qry($contact_qry, $contact_params);
					$contact_params[':uid'] = $new_mem->id();
					$contact_params[':cid'] = $ref_mem->id();
					Powon::qry($contact_qry, $contact_params);
					Powon::redirect(POWON_SUBDIR . $new_mem->action_link(''));
				}
				else
				{
					$new_mem->_edit->error('Invalid referer.');
				}
			}
			$vars = array(
				'login_form' => FALSE,
				'title' => 'Join POWON',
				'form' => $new_mem->_edit->generate(),
			);
			Powon::render('generic-form.tpl', $vars);
		}
	}

	/**
	 * Deletion
	 */
	static function confirm_delete($id) { parent::confirm_delete($id, 'uID', 'Member'); }

	/**
	 * Creation of a new permission
	 */
	function add_perm()
	{
		if ($this->access('edit'))
		{
 			$pid = $this->query("SELECT MAX(permID) FROM MemberPerm WHERE uID = :id")->fetchColumn();
 			$pid++;
 			$this->query("INSERT INTO MemberPerm (uID, permID) VALUES (:id, {$pid})");
 			return $pid;
		}
		return NULL;
	}

	/**
	 * Perform some extra operations when a new Member is saved
	 *
	 * @param array $data
	 *   Array of data to save
	 */
	function save($data=NULL)
	{   
		$new = ($this->id() == NULL);
		parent::save($data);
		if ($new)
		{
			// Create default member contact categories
		  	foreach (self::$default_cats as $i => $name)
		  	{
		  		$name = is_null($name) ? 'NULL' : "'{$name}'";
				Powon::qry("INSERT INTO MemberContactCat VALUES({$this->id()}, {$i}, {$name}, {$i})");
				Powon::qry("INSERT INTO MemberPerm (uID, permID) VALUES({$this->id()}, {$i})");
			}
			// Create primary address
			Powon::qry("INSERT INTO Location (label) VALUES('Primary')");
			$primary_loc = Powon::db_last_id("`Location`.`locID`");
			Powon::qry("INSERT INTO MemberLocation VALUES({$this->id()}, {$primary_loc}, 1, 1)");
		}
	}

	/**
	 * Properly delete a member
	 *
	 * Media created by the member, as well as invoices to the member are
	 * conserved and must be deleted individually.
	 */
	function del()
	{
		$this->query("DELETE FROM MemberContact WHERE uID = :id");
		$this->query("DELETE FROM MemberContact WHERE cID = :id");
		$this->query("DELETE FROM MemberPerm WHERE uID = :id");
		$this->query("DELETE FROM MemberContactCat WHERE uID = :id");
		$this->query("DELETE FROM MemberInterest WHERE uID = :id");
		$this->query("DELETE FROM MemberLocation WHERE uID = :id");
		foreach ($this->_linked['Location']->items() as $loc)
			$loc->del();
		$this->query("DELETE FROM Message WHERE `from` = :id");
		$this->query("DELETE FROM MemberMessage WHERE `from` = :id");
		$this->query("DELETE FROM MemberMessage WHERE `to` = :id");
		$this->query("DELETE FROM MemberMedia WHERE uID = :id");
		$this->query("DELETE FROM GroupMember WHERE uID = :id");
		parent::del();
	}

	/**
	 * Edit form display override
	 */
	function form($template='member-form.tpl', $vars = array())
	{
		$vars = array_merge($vars, $this->ui_tpl_vars());
		parent::form($template, $vars);
	}

	/**
	 * Retrieve the form object
	 */
	function get_form() { return $this->_edit; }

	/**
	 * Alias to display tables within a member context
	 */
	protected function display_linked_table($linkey, $exclude, $title, $ops=array(), $override=array())
	{
		$this->_linked[$linkey]->display_table(
			$exclude, 
			$this->format_title($title),
			$ops,
			$override,
			'member-edit-table.tpl',
			$this->ui_tpl_vars()
		);
	}

	/**
	 * Retrieve member group permissions
	 */
	function member_group_perms()
	{
		if (!isset($this->_linked['GroupMember']))
			$this->add_link(
				'GroupMember',
				new Powon_Entity_Collection(
					'GroupMember',
					'SELECT g.name, mp.* FROM MemberPerm mp NATURAL JOIN GroupMember gm JOIN `Group` g ON g.gID = gm.gID WHERE mp.uID = ' . $this->id(),
					TRUE,
					'Powon_Member_Perm',
					TRUE
				)
			);
		return $this->_linked['GroupMember'];
	}

	/**
	 * Edit profile access control
	 */
	static function edit_access($id)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('edit')))
		{
			$gform = $member->member_group_perms()->form();
			$mform = $member->_linked['MemberContactCat']->form();
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$data = array();
				if(isset($_POST["edit-MemberContactCat-collection-submit"]))
				{
					$mform->validate();
					$data = $mform->data();
				}
				else if(isset($_POST["edit-GroupMember-collection-submit"]))
				{
					$gform->validate();
					$data = $gform->data();
				}
				foreach ($data as $submitted)
				{
					$sql = 'UPDATE MemberPerm SET ';
					foreach ($submitted as $key => $val)
					{
						if ($key != 'uID' && $key != 'permID' && $key != 'catID' && $key != 'name')
							$sql.= "$key='$val',";
					}
					$sql = rtrim($sql, ',');
					$sql .= " WHERE uID = :id AND permID = " . $submitted['permID'];
					$member->query($sql);
				}
				
			}
	
			$vars = $member->ui_tpl_vars();
			$vars['title'] = $member->format_title(' - Profile Privacy');
			$vars['member_edit_sections'][] = array(
				'title' => "Contacts",
				'form' => $mform->generate_table(array('name')),
			);
			$vars['member_edit_sections'][] = array(
				'title' => "Group Members",
				'form' => $gform->generate_table(array('name')),
			);
			Powon::render('member-edit.tpl', $vars);
		}
	}

	/**
	 * Edit member addresses
	 */
	static function edit_interests($id)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('edit')))
		{
			$form = Powon_Form::add_form_instance('tag-input-autocomplete', '/json/tags');
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$form->validate();
				$interest = new Powon_Interest(NULL);
				if (!Powon_Interest::in_db('label', $form->data('tag-input-autocomplete')))
					
					$interest->save(array('label' => $form->data('tag-input-autocomplete')));
				else
					$interest->find_id('label', $form->data('tag-input-autocomplete'));
				$member->query("INSERT INTO MemberInterest (uID, iID) VALUES (:id, {$interest->id()})");
				$member->_linked['Interest']->fetch_all();	
			}

			$items = $member->_linked['Interest']->format_items(
				array('user' => array('del/interest/' => 'Remove'))
			);
			$vars = $member->ui_tpl_vars();
			$vars['title'] = $member->format_title(' - Interests');
			$vars['inline_js'] = $form->inline_script;
			$vars['member_edit_sections'] = array(array('item_list' => $items, 'form' => $form->generate()));
			Powon::render('member-edit.tpl', $vars);
		}
	}

	/**
	 * Edit member addresses
	 */
	static function edit_address($id)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('edit')))
		{
			$vars = $member->ui_tpl_vars();
			$vars['title'] = $member->format_title(' - Address');
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$member->_location->_edit->validate();
				$data = $member->_location->_edit->data();
				$member->_location->save($data);
			}
			$member->_location->form('member-form.tpl', $vars);
		}
	}

	/**
	 * Manage member contacts
	 */
	static function manage_contacts($id)
	{
		$member = self::instance($id);
		if (Powon::access_found($member->found(), $member->access('edit')))
		{
			$member->display_linked_table(
				'MemberContact',
				array('uID'), 
				'- Contacts',
				array(
					'entity' => array('' => 'Go'), 
					'user' => array('del/contact/' => 'Remove')
				),
				array('catID' => array($member, 'select_category_form'))
			);
		}
	}

	/**
	 * Save a contact
	 */
	static function save_contact($id, $cid, $new=FALSE)
	{
		$member = self::instance($id);
		if ($member->access('edit') && $_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$contact = self::instance($cid);
			$form = $member->select_category_form(NULL, $contact, FALSE);
			$form->validate();
			if ($new)
				$sql = "INSERT INTO MemberContact VALUES (:uid, :cid, '{$form->data('contact-category')}')";
			else
				$sql = "UPDATE MemberContact SET catID = {$form->data('contact-category')} WHERE uID = :uid AND cID = :cid";
			Powon::qry($sql,
				array(
					':uid' => $member->id(),
					':cid' => $contact->id(),
				));
		}
		Powon::redirect();
	}

	/**
	 * Remove a contact
	 */
	static function del_contact($id, $cid)
	{
		$member = self::instance($id);
		if ($member->access('edit'))
			$member->_linked['MemberContact']->del_item($cid);
		Powon::redirect();
	}

	/**
	 * Delete an interest
	 */
	static function del_interest($id, $int)
	{
		$member = self::instance($id);
		if ($member->access('edit'))
			$member->_linked['Interest']->del_item($int);
		Powon::redirect();
	}



	/**
	 * Add a contact
	 */
	static function add_contact($id, $cid) { self::save_contact($id, $cid, TRUE); }

	/**
	 * Override action links
	 */
	function action_link($op) { return "/member/{$this->id()}/{$op}"; }

	/**
	 * Generate a title
	 *
	 * @retval string
	 *   A title for this entity
	 */
	function format_title($suffix = "")
	{
		$title = $this->name;
		if ($this->access('view', 'Fullname'))
			$title .= " {$this->midname} {$this->surname}";
		return $title . " $suffix";
	}

	/**
	 * Contact category selection
	 */
	function select_category_form($default=NULL, $for=NULL, $gen=TRUE, $add=FALSE)
	{
		//static $form;
		if (!isset($form))
		{
			if ($add)
				$action = (is_null($for)) ? 'add/contact' : 'add/contact/' . $for->id();
			else
				$action = (is_null($for)) ? 'edit/contact' : 'edit/contact/' . $for->id();
			$submit = ($add) ? 'Add' : 'Save';
			$form = new Powon_Form('powon-contact-category-form', $submit, FALSE, $this->action_link($action) . Powon::redirect_value());
			$form->add('select', 'contact-category', $default, '', $this->_linked['MemberContactCat']->get_values('name'));	
		}
		if ($gen)
			return $form->generate();
		else
			return $form;
	}



	/**
	 * Retrieve information about the relationship of the current user to this member
	 */
	function current_relationship(&$user = NULL)
	{
		//static $output = NULL;
		$output = NULL;
		if (is_null($user))
			$user =& Powon::current_user();
		if ($user->is_user())
		{
			$req = $user->query("SELECT permID FROM MemberContactCat mcc JOIN MemberContact mc ON mcc.catID = mc.catID WHERE mcc.uID = '{$this->id()}' AND mc.cID = :id");
			$pid = ($row = $req->fetch(PDO::FETCH_ASSOC)) ? $row['permID'] : FALSE;

			if (!$pid)
			{
				$req = $this->query("SELECT gm1.permID FROM GroupMember gm1 JOIN GroupMember gm2 ON gm2.gID = gm1.gID WHERE gm1.uID != gm2.uID AND gm2.uID = {$user->id()} and gm1.uID = :id");
				$pid = ($row = $req->fetch(PDO::FETCH_ASSOC)) ? $row['permID'] : FALSE;
				if ($pid)
					$output = $this->member_group_perms->item($pid);
			}
		}
		if (is_null($output) && isset($this->_linked['MemberContactCat']))
			$output = $this->_linked['MemberContactCat']->item(POWON_DEFAULT_USER_RELATIONSHIP);
		return $output;
	}

	/**
	 * Member access check
	 *
	 * @param string $op
	 *   Operation being requested
	 * @param string $item
	 *   Specific member property being accessed
	 * @retval boole
	 *   Whether the current user can access the requested operation for the entity
	 */
	function access($op='view', $item=NULL)
	{
		$user = Powon::current_user();
		$acc = FALSE;
		if ($user->id() == $this->id())
			$acc = TRUE;
		else
			switch ($op)
			{
				case 'view':
					if (is_null($item))
						$acc = $user->is_user();
					else
						$acc = (is_null($this->current_relationship($user))) ? FALSE : $this->current_relationship($user)->check($op . $item);
					break;

				case 'new':
					if ($user->is_admin())
						$acc = TRUE;
					break;

				case 'edit':
				case 'admin':
					$acc = (!is_null($this->id()) && $user->is_admin());
					break;
			}
		return $acc;
	}

	/**
	 * Access sensitive stringify
	 */
	function format_attr($label, $value, $wrap='')
	{   
		if ($this->attr_access($label))
			return parent::stringify($label, $value, $wrap);
		else
			return "";
	}

	/**
	 * Label based access control
	 */
	function attr_access($item)
	{
		if (Powon::current_user()->is_admin())
			return TRUE;
		switch (strtolower($item))
		{
			case 'email':
				return $this->access('view', 'Email');
				break;

			case 'dob':
				return $this->access('view', 'DOB');
				break;

			case 'address':
			case 'city':
			case 'region':
			case 'prov':
			case 'country':
			case 'code':
			case 'street':
				return $this->access('view', 'Address');
				break;

			case 'midname':
			case 'surname':
				return $this->access('view', 'Fullname');
				break;
	
			default:
				return TRUE;
				break;
		}
	}

	/**
	 * Is the supplied member or user a contact of this member
	 */
	function is_contact($mem=NULL)
	{
		$mem = (is_null($mem)) ? Powon::current_user() : $mem;
		return in_array($mem->uID, array_keys($this->_linked['MemberContact']->items()));
	}

	/**
	 * Member menu generation
	 */
	protected function edit_links()
	{
		return array(
			'post' => 'Post',
			'edit' => 'Profile',
			'edit/access' => 'Privacy',
			'edit/address' => 'Address',
			'edit/contact' => 'Contacts',
			'edit/interest' => 'Interests',
			'' => 'View',
		);
	}

	/**
	 * tpl vars for user interface
	 */
	private function ui_tpl_vars()
	{
		$user = Powon::current_user();
		return array(
			'is_user_profile' => ($user->uID == $this->id()),
			'submenu' => ($this->access('edit')) ? $this->format_action_list($this->edit_links(), FALSE) : "",
		);
	}

	/**
	 * Register template variables
	 *
	 * @retval array
	 *   key value pairs for assignment to a view
	 */
	function template_vars()
	{
		$user = Powon::current_user();
		$action_add = NULL;
		if ($this->uID != $user->id())
			$action_add = ($user->member()->is_contact($this)) ? $user->member()->format_action('del/contact/' . $this->uID, 'Remove from contacts') : $user->member()->select_category_form(NULL, $this, TRUE, TRUE);
		$vars = array(
			'posts' => $this->format_posts(),
			'is_user_profile' => ($user->uID == $this->id()),
			'member_avatar' => is_null($this->avatar()) ? '' : $this->avatar()->display,
			'title' => $this->format_title(),
			'member_name' => $this->format_title(),
			'member_since' => Powon::format_since($this->joined),
			'member_dob' => ($this->access('view', 'DOB')) ? $this->dob->format("j F, Y") : NULL,
			'member_email' => ($this->access('view', 'Email')) ? $this->email : NULL,
			'submenu' => $action_add.$this->format_action('msg', 'Send a message'),
			'member_interests' => $this->_linked['Interest']->format_items(),
		);
		if ($this->access('edit'))
			$vars['submenu'] .= $this->format_action('edit');
		if ($this->access('view', 'Address') && isset($this->_location) && $this->_location->is_public())
			$vars = array_merge($vars, $this->_location->embed_template_vars());
		
		return $vars;
	}

	/**
	 * Display a member
	 *
	 * @param integer $id
	 *   Unique id of the member
	 */
	static function view($id) 
	{ 
		if (is_numeric($id)) 
			parent::view($id, 'uID', 'Member');
		else
			return TRUE;
	}


}

?>
