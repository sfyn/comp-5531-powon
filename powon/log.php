<?php
/**
 * @file log.php
 * Provide a logger object for recording histories
 */

/**
 * @class
 * Log
 */
class Powon_Log
{
	private $_times;
	private $_lines;

	/** 
	 * Constructor
	 */
	function __construct()
	{
		$this->_times = array();
		$this->_lines = array();
	}

	/**
	 * Destructor
	 */
	function __destruct() {}

	/**
	 * Return all lines 
	 */
	function lines()
	{
		return $this->_lines;	
	}

	/**
	 * Log a line
	 *
	 * @param string $l
	 *   Line to log
	 */
	function wl($l)
	{
		$this->_times[] = time();
		$this->_lines[count($this->_times)-1] = $l;
	}

	/**
	 * Format a specific line
	 *
	 * Provided formats:
	 *   - 'txt' - plain text, time and log line seperated by a tab
	 *
	 * @param int $i
	 *   Index in log to format
	 * @param string
	 *   What kind of format to use
	 * @retval string
	 *   A printable string
	 */
	function format($i, $f='txt')
	{
		$output = "";
		switch ($f)
		{
			case 'txt':
			default:
				$output = date(DATE_ATOM, $this->_times[$i]) . " \t" . $this->_lines[$i];
				break;
		}
		return $output;
	}

	/**
	 * Print a specific line
	 *
	 * @param int $i
	 *   Reverse index of the line (0 for last line, -1 for second-last, etc)
	 */
	function pl($i=0)
	{
		$i--;
		print $this->format(count($this->_times)+$i);
	}
}
?>
