-- MySQL dump 10.13  Distrib 5.5.50, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: comp5531_powon
-- ------------------------------------------------------
-- Server version	5.5.50-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Activity`
--

DROP TABLE IF EXISTS `Activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Activity` (
  `gID` bigint(20) NOT NULL,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `gID` (`gID`),
  KEY `start` (`start`),
  KEY `end` (`end`),
  CONSTRAINT `Activity_ibfk_1` FOREIGN KEY (`gID`) REFERENCES `Group` (`gID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Group`
--

DROP TABLE IF EXISTS `Group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group` (
  `gID` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `avatar` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`gID`),
  UNIQUE KEY `name` (`name`),
  KEY `avatar` (`avatar`),
  CONSTRAINT `Group_ibfk_1` FOREIGN KEY (`avatar`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GroupInterest`
--

DROP TABLE IF EXISTS `GroupInterest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupInterest` (
  `gID` bigint(20) NOT NULL,
  `iID` bigint(20) NOT NULL,
  PRIMARY KEY (`gID`,`iID`),
  KEY `iID` (`iID`),
  CONSTRAINT `GroupInterest_ibfk_1` FOREIGN KEY (`gID`) REFERENCES `Group` (`gID`),
  CONSTRAINT `GroupInterest_ibfk_2` FOREIGN KEY (`iID`) REFERENCES `Interest` (`iID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GroupMedia`
--

DROP TABLE IF EXISTS `GroupMedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupMedia` (
  `gID` bigint(20) NOT NULL,
  `dID` bigint(20) NOT NULL,
  PRIMARY KEY (`gID`,`dID`),
  KEY `dID` (`dID`),
  CONSTRAINT `GroupMedia_ibfk_1` FOREIGN KEY (`gID`) REFERENCES `Group` (`gID`),
  CONSTRAINT `GroupMedia_ibfk_2` FOREIGN KEY (`dID`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GroupMember`
--

DROP TABLE IF EXISTS `GroupMember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupMember` (
  `uID` bigint(20) NOT NULL,
  `gID` bigint(20) NOT NULL,
  `role` varchar(255) DEFAULT 'member',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `permID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uID`,`gID`),
  KEY `gID` (`gID`),
  KEY `role` (`role`),
  CONSTRAINT `GroupMember_ibfk_1` FOREIGN KEY (`gID`) REFERENCES `Group` (`gID`),
  CONSTRAINT `GroupMember_ibfk_2` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GroupMessage`
--

DROP TABLE IF EXISTS `GroupMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupMessage` (
  `to` bigint(20) NOT NULL,
  `from` bigint(20) NOT NULL,
  `mID` bigint(20) NOT NULL,
  PRIMARY KEY (`to`,`from`,`mID`),
  KEY `from` (`from`,`mID`),
  CONSTRAINT `GroupMessage_ibfk_1` FOREIGN KEY (`to`) REFERENCES `Group` (`gID`),
  CONSTRAINT `GroupMessage_ibfk_2` FOREIGN KEY (`from`, `mID`) REFERENCES `Message` (`from`, `mID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Interest`
--

DROP TABLE IF EXISTS `Interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Interest` (
  `iID` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `description` text,
  `avatar` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`iID`),
  UNIQUE KEY `label` (`label`),
  KEY `avatar` (`avatar`),
  CONSTRAINT `Interest_ibfk_1` FOREIGN KEY (`avatar`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Invoice`
--

DROP TABLE IF EXISTS `Invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Invoice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `uID` bigint(20) NOT NULL,
  `emitted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `due` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `paid` tinyint(1) DEFAULT '0',
  `data` mediumblob,
  PRIMARY KEY (`ID`),
  KEY `emitted` (`emitted`),
  KEY `due` (`due`),
  KEY `paid` (`paid`),
  KEY `uID` (`uID`),
  CONSTRAINT `Invoice_ibfk_1` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Location`
--

DROP TABLE IF EXISTS `Location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Location` (
  `locID` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `street` varchar(511) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `prov` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`locID`),
  KEY `street` (`street`(255)),
  KEY `city` (`city`),
  KEY `region` (`region`),
  KEY `prov` (`prov`),
  KEY `country` (`country`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Media`
--

DROP TABLE IF EXISTS `Media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Media` (
  `dID` bigint(20) NOT NULL AUTO_INCREMENT,
  `creator` bigint(20) DEFAULT '0',
  `type` varchar(255) DEFAULT 'image',
  `label` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT 'local',
  `uri` varchar(255) DEFAULT NULL,
  `display` mediumtext,
  PRIMARY KEY (`dID`),
  KEY `type` (`type`),
  KEY `label` (`label`),
  KEY `source` (`source`),
  KEY `creator` (`creator`),
  CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MediaComments`
--

DROP TABLE IF EXISTS `MediaComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MediaComments` (
  `dID` bigint(20) NOT NULL,
  `comID` bigint(20) NOT NULL,
  `uID` bigint(20) NOT NULL,
  `text` mediumtext,
  PRIMARY KEY (`dID`,`comID`),
  KEY `uID` (`uID`),
  CONSTRAINT `MediaComments_ibfk_1` FOREIGN KEY (`dID`) REFERENCES `Media` (`dID`),
  CONSTRAINT `MediaComments_ibfk_2` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MediaPerm`
--

DROP TABLE IF EXISTS `MediaPerm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MediaPerm` (
  `dID` bigint(20) NOT NULL,
  `permID` bigint(20) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1',
  `comment` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dID`,`permID`),
  CONSTRAINT `MediaPerm_ibfk_1` FOREIGN KEY (`dID`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Member`
--

DROP TABLE IF EXISTS `Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member` (
  `uID` bigint(20) NOT NULL AUTO_INCREMENT,
  `pwd` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `midname` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `status` varchar(255) DEFAULT 'active',
  `priv` varchar(255) DEFAULT 'member',
  `avatar` bigint(20) DEFAULT NULL,
  `joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uID`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `avatar` (`avatar`),
  KEY `name` (`name`),
  KEY `midname` (`midname`),
  KEY `surname` (`surname`),
  CONSTRAINT `Member_ibfk_1` FOREIGN KEY (`avatar`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberContact`
--

DROP TABLE IF EXISTS `MemberContact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberContact` (
  `uID` bigint(20) NOT NULL DEFAULT '0',
  `cID` bigint(20) NOT NULL DEFAULT '0',
  `catID` int(11) DEFAULT NULL,
  PRIMARY KEY (`uID`,`cID`),
  KEY `MemberContact_fk_cID` (`cID`),
  KEY `catID` (`catID`),
  CONSTRAINT `MemberContact_fk_cID` FOREIGN KEY (`cID`) REFERENCES `Member` (`uID`),
  CONSTRAINT `MemberContact_fk_uID` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberContactCat`
--

DROP TABLE IF EXISTS `MemberContactCat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberContactCat` (
  `uID` bigint(20) NOT NULL DEFAULT '0',
  `catID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `permID` int(11) DEFAULT NULL,
  PRIMARY KEY (`uID`,`catID`),
  KEY `name` (`name`),
  CONSTRAINT `MemberContactCat_fk_uID` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberInterest`
--

DROP TABLE IF EXISTS `MemberInterest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberInterest` (
  `uID` bigint(20) NOT NULL,
  `iID` bigint(20) NOT NULL,
  PRIMARY KEY (`uID`,`iID`),
  KEY `iID` (`iID`),
  CONSTRAINT `MemberInterest_ibfk_1` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`),
  CONSTRAINT `MemberInterest_ibfk_2` FOREIGN KEY (`iID`) REFERENCES `Interest` (`iID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberLocation`
--

DROP TABLE IF EXISTS `MemberLocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberLocation` (
  `uID` bigint(20) NOT NULL,
  `locID` bigint(20) NOT NULL,
  `isPrivate` tinyint(1) DEFAULT '1',
  `isPrimary` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`uID`,`locID`),
  KEY `isPrivate` (`isPrivate`),
  KEY `isPrimary` (`isPrimary`),
  KEY `locID` (`locID`),
  CONSTRAINT `MemberLocation_ibfk_1` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`),
  CONSTRAINT `MemberLocation_ibfk_2` FOREIGN KEY (`locID`) REFERENCES `Location` (`locID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberMedia`
--

DROP TABLE IF EXISTS `MemberMedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberMedia` (
  `uID` bigint(20) NOT NULL,
  `dID` bigint(20) NOT NULL,
  PRIMARY KEY (`uID`,`dID`),
  KEY `dID` (`dID`),
  CONSTRAINT `MemberMedia_ibfk_1` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`),
  CONSTRAINT `MemberMedia_ibfk_2` FOREIGN KEY (`dID`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberMessage`
--

DROP TABLE IF EXISTS `MemberMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberMessage` (
  `to` bigint(20) NOT NULL,
  `from` bigint(20) NOT NULL,
  `mID` bigint(20) NOT NULL,
  `unread` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`to`,`from`,`mID`),
  KEY `msg` (`from`,`mID`),
  CONSTRAINT `MemberMessage_ibfk_1` FOREIGN KEY (`to`) REFERENCES `Member` (`uID`),
  CONSTRAINT `msg` FOREIGN KEY (`from`, `mID`) REFERENCES `Message` (`from`, `mID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MemberPerm`
--

DROP TABLE IF EXISTS `MemberPerm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberPerm` (
  `uID` bigint(20) NOT NULL DEFAULT '0',
  `permID` int(11) NOT NULL DEFAULT '0',
  `viewAll` tinyint(1) NOT NULL DEFAULT '0',
  `viewFullname` tinyint(1) NOT NULL DEFAULT '0',
  `viewAddress` tinyint(1) NOT NULL DEFAULT '0',
  `viewDOB` tinyint(1) NOT NULL DEFAULT '0',
  `viewEmail` tinyint(1) NOT NULL DEFAULT '0',
  `viewMedia` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uID`,`permID`),
  CONSTRAINT `MemberPerm_ibfk_1` FOREIGN KEY (`uID`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Message`
--

DROP TABLE IF EXISTS `Message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Message` (
  `from` bigint(20) NOT NULL,
  `mID` bigint(20) NOT NULL,
  `content` longtext,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`from`,`mID`),
  CONSTRAINT `Message_ibfk_1` FOREIGN KEY (`from`) REFERENCES `Member` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PowonMedia`
--

DROP TABLE IF EXISTS `PowonMedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PowonMedia` (
  `dID` bigint(20) NOT NULL,
  PRIMARY KEY (`dID`),
  CONSTRAINT `PowonMedia_ibfk_1` FOREIGN KEY (`dID`) REFERENCES `Media` (`dID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-15 20:45:51
