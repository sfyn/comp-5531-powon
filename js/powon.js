$(document).ready(function() {

	/**
 	 * Activite slider elements
 	 */
	$('.slider-wrapper').children('.slider-toggle').click(function(e){
		$(this).siblings('.slider').slideToggle();
	});
	$('.slider-wrapper .slider').toggle();
	$('.slider-wrapper .slider-toggle').css('cursor', 'pointer');

	/**
	 * Login form toggle
	 */
	$('#toggle-login-form').click(function(e){
		e.preventDefault();
		$('#powon-login-form').slideDown();
	});
	$('#toggle-login-form').toggle();
	$('#powon-login-form').toggle();

});
