<?php

	function cssOut()
	{
		$out = "<style>\n";
		$out .= "\t th.label { text-align: right; }\n";
		$out .= "\t td.value { text-align: left; }\n";
		$out .= "</style>\n";
		return $out;
	}

	function headOut($title = 'Powan requirements')
	{
		print "<html>\n\t<head>\n\t\t<title>$title</title>\n" . cssOut() . "\t</head>\n\t<body>\n\t\t<h1>$title</h1>\n";
	}

	function tailOut()
	{
		print "\t</body>\n</html>\n";
	}

	function addRow($label, $value)
	{
		static $tableRows;
		if (!isset($tableRows))
			$tableRows = array();
		$tableRows[$label] = $value;
		return $tableRows;
	}

	function tableOut($rows)
	{
		$table = "<table>\n";
		foreach ($rows as $label => $value)
			$table .= "\t<tr><th class=\"label\">$label</th><td class=\"value\">$value</td></tr>\n";
		$table .= "</table>\n";

		print $table;
	}

	addRow('PHP version', phpversion());

	try
	{
		include (__DIR__ . '/lib/flight/Flight.php');
		Flight::route('/requirements.php', function(){
			addRow('Basic Routing', 'succeeded');
			headOut();
			tableOut(addRow('Advanced Routing', '<a href="requirements.php/foo/bar">Test</a>'));
			tailOut();
		});
		Flight::route('/requirements.php/@foo/@bar', function($foo, $bar){
			addRow('Basic Routing', 'succeeded');
			headOut();
			if ($foo == 'foo' && $bar == 'bar')
				tableOut(addRow('Advanced Routing', 'succeeded'));
			else
				tableOut(addRow('Advanced Routing', 'failed'));
			tailOut();
		});
		Flight::start();
	}
	catch (Exception $e)
	{
		headOut();
		tableOut(addRow('Flight routing', $e->getMessage()));
		tailOut();
	}

?>
