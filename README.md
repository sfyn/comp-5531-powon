POWON
=====

POWON stands for Private Online What’s On Now System (POWON). POWON is a multi-user database application with a web-based GUI. It provides users with the ability to indicate interests, communicate with other users, share content, and create groups of like-minded users.

This project was developed for COMP 5531 : Databases and File Systems, taught at Concordia University, during the summer session of 2016, by Sofian Benaissa, student id# 24670523.

For a detailed description of the system and it's usage, please see the accompanying final report and user manual.

Installation
------------

### Deploy source

To install POWON, first deploy or copy the files to a web directory. Ensure the following files and directories are accessible to the web server:

     index.php
     install.php
     lib
     lib/*
     js
     js/*
     css
     css/*
     powon
     powon/*
     tpl
     tpl/*
     tpl/src/*

In addition, the directory `tpl/comp` must be writable by the web server.

### Set up the database

To set up the database, import the contents of `db/powon.sql` into a mysql server. This can be accomplished from the linux command line with the following command:

     mysql -u username -p"password" -h database.hostname databasename < powon.sql

### Configure POWON

Finally, copy `powon/conf-example.php` to `powon/conf.php` and edit it to reflect the database credentials and other values it requires.

### Download libraries

POWON requires several external libraries. The script `lib/setup.sh` will download them and setup symlinks to allow powon to find the external libraries it uses. You can also manually run these commands - please consult `lib/setup.sh` to see what libraries are needed and how they should be referred to by POWON.

### Create the first user

Point your web browser to your website, and open the uri `install.php` to begin the process of creating the first administrative user on the system.

License
-------

Copyright 2016 Sofian Benaissa.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version. 

This file is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
details. You can consult the license at http://www.gnu.org/licenses/
