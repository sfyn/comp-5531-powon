{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
	<h1>{$title}</h1>
	<dl>
		<dt>From</dt>
		<dd>{$sender}</dd>
		<dt>Sent</dt>
		<dd>{$message.time}</dd>
	</dl>
	<div>
		{$message.content}
	</div>
{/block}
