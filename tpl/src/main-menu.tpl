<nav id='powon-main-menu'>
	<span class="left-side">
		<a href="{$powon_subdir}/" class="menu-item menu-left powon-logo"><img alt="logo" src="{$powon_subdir}/powon/logo/logow.png"></a>
	{if $logged_in}
		<a href="{$powon_subdir}/member" class="menu-item">Contacts</a>
		<a href="{$powon_subdir}/group" class="menu-item">Groups</a>
		<a href="{$powon_subdir}/interest" class="menu-item">Interests</a>
	</span>
	<span class="right-side">
	{if $admin}
		<a href="{$powon_subdir}/admin" class="menu-item">Admin</a>
	{/if}
		<a href="{$powon_subdir}/inbox" class="menu-item menu-right">Inbox</a>
		<a href="{$powon_subdir}/logout" class="menu-item menu-right">Logout</a>
		<a href="{$powon_subdir}/profile" class="menu-item menu-right">{$user_avatar}</a>
	{else}
		<a href="{$powon_subdir}/join" class="menu-item">Join Powon</a>
	</span>
	<span class="right-side">
		<a href="" id="toggle-login-form" class="menu-item hide-me">Login</a>
	{/if}
	</span>
</nav>
{if isset($submenu)}
<nav id="powon-submenu">
	{$submenu}
</nav>
{/if}
{if !$logged_in}
	{$login_form}
{/if}
