{if $data}
<span class="data-line powon-{$class}">
	{if isset($label)}
		<label>{$label}:&nbsp;</label>
	{/if}
	{$data}
</span>
{/if}
