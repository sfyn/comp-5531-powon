<div class="powon-location">
	{include file='data-line.tpl' class='loc-street' data=$loc_street}
	{include file='data-line.tpl' class='loc-city' data=$loc_city}
	{include file='data-line.tpl' class='loc-region' data=$loc_region}
	{include file='data-line.tpl' class='loc-prov' data=$loc_prov}
	{include file='data-line.tpl' class='loc-country' data=$loc_country}
	{include file='data-line.tpl' class='loc-code' data=$loc_code}
</div>
