{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
			{block name=avatar}
			{/block}
			<h1>{$title}</h1>
			{block name=content}
			<div class="powon-actions">
				{$member_action_add}
			</div>
			{include file='data-line.tpl' class='member-since' data="Member for $member_since"}
			{include file='data-line.tpl' class='member-dob' data=$member_dob label='Birth date'}
			{include file='email.tpl' class='member-email' email=$member_email label='Email'}
			{if isset($loc_template)}
				{include file=$loc_template}
			{/if}
			{if !empty($member_interests)}
				<strong>Interests:</strong>
				{foreach $member_interests as $int}
					{$int}
				{/foreach}
			{/if}
			{/block}
{/block}
