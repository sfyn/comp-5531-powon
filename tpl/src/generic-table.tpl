{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
	<h1>{$title}</h1>
	<table>
		<tr class="powon-table-header">
			{foreach $table_header as $label}
				<th>{$label}</th>
			{/foreach}	
			{if isset($table_rows)}{foreach $table_rows as $row}
				{include file='table-row.tpl' class='table-row' row=$row header=$table_header}
			{/foreach}{/if}	
		</tr>
	</table>
	{if isset($addform)}
	<h2>Add:</h2>
	{$addform}{/if}
{/block}
