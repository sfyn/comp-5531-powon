<div class="embedded-post">
<h2>{$post.title}</h2>
<div class="post-links">{$post.links}</div>
<div class="post-content">
{$post.content}
</div>
{foreach $post.comments as $comment}
	<div class="post-comment">
	<span class="comment-author">{$comment.author}</span>{if $comment.del}{$comment.del}{/if}
	<div class="comment-author">{$comment.comment}</div>
	</div>
{/foreach}
</div>
