{extends file="member-layout.tpl"}
{block name=content}
	<table>
		<tr class="powon-table-header">
			{foreach $table_header as $label}
				<th>{$label}</th>
			{/foreach}	
			{if isset($table_rows)}{foreach $table_rows as $row}
				{include file='table-row.tpl' class='table-row' row=$row header=$table_header}
			{/foreach}{/if}
		</tr>
	</table>
{/block}
