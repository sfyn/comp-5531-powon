{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
	<h1>{$title}  {$edit}</h1>
	<div class='described'>{$desc}</div>
	{if !empty($posts)}
	<h2>Posts</h2>
	{foreach $posts as $post}
		{include file='post-embed.tpl' post=$post}
	{/foreach}
	{/if}
	<h2>Members</h2>
		{if $index_links}
		<nav class="powon-index-links">
			{include file='link-set.tpl' links=$index_links}
		</nav>
		{/if}
	<div class="grid-index">
		{foreach $members as $item}
		<div class="grid-item"><a href="{$item.link}">
			{$item.img}
			<label>{$item.label}</label>
		</a></div>
		{/foreach}	
	</div>
{/block}
