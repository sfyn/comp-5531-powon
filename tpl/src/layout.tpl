<html>
	<head>
		<title>{block name=title}Powon{/block}</title>
		<link rel="stylesheet" href="{$powon_subdir}/lib/jquery-ui.css">
		<link rel="stylesheet" href="{$powon_subdir}/css/powon.css">
		<script src="{$powon_subdir}/lib/jquery.min.js"></script>
		<script src="{$powon_subdir}/lib/jquery-ui.min.js"></script>
		<script src="{$powon_subdir}/js/powon.js"></script>
		<script>
			{$inline_js}
		</script>
	</head>
	<body>
		{include file='main-menu.tpl'}
		{if $admin}
		{include file='log.tpl'}
		{/if}
		<main>
			{block name=main}{/block}
		</main>
	</body>
</html>
