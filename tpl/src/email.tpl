{if $email}
<span class="data-line email-address powon-{$class}">
	{if isset($label)}
	    <label>{$label}:&nbsp;</label>
	{/if}
	{mailto address=$email encode='javascript' subject='Hello'}
</span>
{/if}
