	{if isset($sec.title)}
	<h2>{$sec.title}</h2>
	{/if}
	{if isset($sec.table)}
	<table>
		<tr class="powon-table-header">
			{foreach $sec.table.table_header as $label}
				<th>{$label}</th>
			{/foreach}	
			{if isset($sec.table.table_rows)}{foreach $sec.table.table_rows as $row}
				{include file='table-row.tpl' class='table-row' row=$row header=$table_header}
			{/foreach}{/if}
		</tr>
	</table>
	{elseif isset($sec.item_list)}
	<ul>
		{foreach $sec.item_list as $item}
			<li>{$item}</li>
		{/foreach}
	</ul>
	{/if}
	{if isset($sec.form)}
	{$sec.form}
	{/if}
