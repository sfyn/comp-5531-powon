{extends file="member-layout.tpl"}
	{block name=avatar}
	{$member_avatar}
	{/block}
	{block name=content}
	{include file='data-line.tpl' class='member-since' data="Member for $member_since"}
	{include file='data-line.tpl' class='member-dob' data=$member_dob label='Birth date'}
	{include file='email.tpl' class='member-email' email=$member_email label='Email'}
	{if isset($loc_template)}
		{include file=$loc_template}
	{/if}
	{if !empty($member_interests)}
		<strong>Interests:</strong>
		{foreach $member_interests as $int}
			{$int}
		{/foreach}
	{/if}
	{foreach $posts as $post}
		{include file='post-embed.tpl' post=$post}
	{/foreach}
	{/block}
