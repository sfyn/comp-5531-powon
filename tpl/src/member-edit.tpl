{extends file="member-layout.tpl"}
{block name=content}
	{foreach $member_edit_sections as $sec}
		{include file='member-edit-section.tpl' sec=$sec}
	{/foreach}
{/block}
