{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
	<h1>{$title}</h1>
		{if $index_links}
		<nav class="powon-index-links">
			{include file='link-set.tpl' links=$index_links}
		</nav>
		{/if}
	<table>
		<tr class="powon-table-header">
			{foreach $table_header as $label}
				<th>{$label}</th>
			{/foreach}	
			{if isset($table_rows)}{foreach $table_rows as $row}
				{include file='table-row.tpl' class='table-row' row=$row header=$table_header}
			{/foreach}{/if}
		</tr>
	</table>
{/block}
