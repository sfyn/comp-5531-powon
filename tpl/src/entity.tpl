{extends file="layout.tpl"}
{block name=title}{$entity_title}{/block}
{block name=main}
			<h1>{$entity_title}</h1>
			<dl>
			{foreach $entity_data as $label => $value}
				<dt>{$label}</dt>
				<dd>{$value}</dd>
			{/foreach}
			</dl>
{/block}
