{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
	<h1>{$title}</h1>
		{if $index_links}
		<nav class="powon-index-links">
			{include file='link-set.tpl' links=$index_links}
		</nav>
		{/if}
	<div class="grid-index">
		{foreach $grid_items as $item}
		<div class="grid-item"><a href="{$item.link}">
			{$item.img}
			<label>{$item.label}</label>
		</a></div>
		{/foreach}	
	</div>
{/block}
