{extends file="layout.tpl"}
{block name=title}{$title}{/block}
{block name=main}
	<h1>{$title}</h1>
	{foreach $posts as $post}
		{include file='post-embed.tpl' post=$post}
	{/foreach}
{/block}
